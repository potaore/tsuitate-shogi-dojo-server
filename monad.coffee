#IdentityMonad
class Id
	constructor : (@value) ->
	bind : (func) => func(@value)

Object.prototype.toId = () -> new Id(@);

asdf = (12).toId().bind( (x) -> (13).toId().bind( (y) -> x + y) )
console.log  asdf


chain = (value) -> (func) -> 
	cont    : () -> chain( func(value) )
	listen  : (eventName) -> (func2) -> 
		execute : chain

class EventObject
	constructor : (@value) ->
	exec : (func) => @value = func(@value)
	listen 

reduce = (init, func) -> (val) -> { 
	end : () -> func(init, val)
	next : (val2) -> reduce( func(init, val), func )( val2 ) }
cur1 = (func) -> (x) -> (y) -> func(x, y)


chain(12)( 
	(x) -> x + 12 )( 
	(y) -> y + 12 )(
	(z) -> console.log z
)

aaa = reduce(1, (x, y) -> x + y)(12).next(2).next(3).next(4).end()
console.log aaa


class Node
  constructor : () -> 
  	@sockets = {}
  	@pointcuts = {}
  connect     : (params, cont) -> 
    socket = new Socket(@, params)
    @sockets[socket.id] = socket
    cont(socket)

  addPointcut : (name, cont) ->
  	pointcut = new Pointcut()
  	@pointcuts.push pointcut
  	cont(pointcut)

  g$emitter : (socketGuard) -> 
    emit : (name, arg, cont) => 
      _( pointcut ).each( (pc) -> 
        pc(name, arg)
      )
      console.log "emit : #{name}"
      console.log arg
      console.log cont
	  _( _( _(@sockets).values() ).filter(socketGuard) ).each( (socket) -> 
	  	result = socket.fire(name, arg, cont)
	  )

class Socket
  constructor : (@node, @params) -> 
    @id = id = uuid()
    @events = {}
    @broadcast = @node.g$emitter( (socket) -> socket.id isnt id )
    id  = null
  on   : (name, func)  -> @events[name] = func
  fire : (name, arg, cont) -> 
    if @events[name]
      ret = @events[name](arg) 
      cont?(ret)
  to   : (id) -> @node.g$emitter( (socket) -> socket.id is id )

class Pointcut
  constructor : (@node, @params) -> 