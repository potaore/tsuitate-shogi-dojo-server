_ = require('underscore')

util =
  isVacant : (val) -> val is null or val is undefined or val is "" or val is 0
  isntVacant : (val) -> val isnt null and val isnt undefined and val isnt "" and val isnt 0
  reverseVector : (position, playerNumber) ->
    return position if playerNumber is 1 
    return x : position.x * -1, y : position.y * -1, length : position.length
  posEq  : (p1, p2) -> p1.x is p2.x and p1.y is p2.y
  posAdd : (p, v) -> {x : p.x + v.x, y : p.y + v.y, length : v.length}
  posMul : (p, times) -> { x : p.x * times, y : p.y * times, length : times }
  posDup : (p) -> { x : p.x , y : p.y, length : p.length}
  posAdd1 : (p) -> { x : p.x + 1 , y : p.y + 1, length : p.length}
  posToStr : (p) -> p.x+":"+p.y
  isInRange : (pos) -> 0 <= pos.x and pos.x < 9 and 0 <= pos.y and pos.y < 9
  allPos : (predicate) ->
    for y in [0...9]
      for x in [0...9]
        predicate(x, y)

messages = {
  game_start               : "対局開始です"
  turn_own                 : "あなたの手番です"
  turn_opp                 : "相手の手番です"
  get_koma_own             : "を取りました"
  get_koma_opp             : "がとられました"
  oute_own                 : "王手をかけました"
  oute_opp                 : "王手をかけられました"
  foul_own                 : "反則手です"
  foul_opp                 : "相手が反則手を指しました"
  win_own                  : "あなたの勝ちです"
  win_opp                  : "あなたの負けです"
  win_p1                   : "先手の勝ちです"
  win_p2                   : "後手の勝ちです"
  win_foul_own             : "反則回数超過であなたの勝ちです"
  win_foul_opp             : "反則回数超過であなたの負けです"
  win_foul_p1              : "反則回数超過で先手の勝ちです"
  win_foul_p2              : "反則回数超過で後手の勝ちです"
  win_timeout_own          : "時間切れであなたの勝ちです"
  win_timeout_opp          : "時間切れであなたの負けです"
  win_timeout_p1           : "時間切れで先手の勝ちです"
  win_timeout_p2           : "時間切れで後手の勝ちです"
  win_resign_own           : "投了であなたの勝ちです"
  win_resign_opp           : "投了であなたの負けです"
  win_resign_p1            : "投了で先手の勝ちです"
  win_resign_p2            : "投了で後手の勝ちです"
  win_discconect_own       : "接続切れであなたの勝ちです"
  win_discconect_opp       : "接続切れであなたの負けです"
  win_discconect_p1        : "接続切れで先手の勝ちです"
  win_discconect_p2        : "接続切れで後手の勝ちです"
}

class KomaType
  constructor : (@id, @name, @nari, @resourceId, @nariId, @moveDef, @value) ->

komaTypes = ( ->
  _komaTypes = {}
  setResource = (id, name, nari, resourceId, nariId, moveDef, value) ->
    _komaTypes[id] = new KomaType(id, name, nari, resourceId, nariId, moveDef, value)

  setResource("ou", "玉"  , false, "ou", ""  , { str : "12346789"     , moves : ["1","2","3","4","6","7","8","9"]}    , 9)
  setResource("ki", "金"  , false, "ki", ""  , { str : "123468"       , moves : ["1","2","3","4","6","8"]}            , 5)
  setResource("gi", "銀"  , true , "gi", "ng", { str : "12379"        , moves : ["1","2","3","7","9"]}                , 4)
  setResource("ng", "成銀", false, "gi", ""  , { str : "123468"       , moves : ["1","2","3","4","6","8"]}            , 4)
  setResource("ke", "桂"  , true , "ke", "nk", { str : "ab"           , moves : ["a","b"]}                           , 3)
  setResource("nk", "成桂", false, "ke", ""  , { str : "123468"       , moves : ["1","2","3","4","6","8"]}            , 3)
  setResource("ky", "香"  , true , "ky", "ny", { str : "2_"           , moves : ["2_"]}                               , 2)
  setResource("ny", "成香", false, "ky", ""  , { str : "123468"       , moves : ["1","2","3","4","6","8"]}            , 2)
  setResource("hi", "飛"  , true , "hi", "ry", { str : "2_4_6_8_"     , moves : ["2_","4_","6_","8_"]}                , 7)
  setResource("ry", "竜"  , false, "hi", ""  , { str : "12_34_6_78_9" , moves : ["1","2_","3","4_","6_","7","8_","9"]}, 7)
  setResource("ka", "角"  , true , "ka", "um", { str : "1_3_7_9_"     , moves : ["1_","3_","7_","9_"]}                , 6)
  setResource("um", "馬"  , false, "ka", ""  , { str : "1_23_467_89_" , moves : ["1_","2","3_","4","6","7_","8","9_"]}, 6)
  setResource("hu", "歩"  , true , "hu", "to", { str : "2"            , moves : ["2"]}                                , 1)
  setResource("to", "と"  , false, "hu", ""  , { str : "123468"       , moves : ["1","2","3","4","6","8"]}            , 1)
  _komaTypes)()

getKomaName = (id) ->
  if( komaTypes[id].resourceId )
    komaTypes[ komaTypes[id].resourceId ].name
  else
    komaTypes[id].name

class MoveConvertor
  constructor : ->
    create8Aray = (v) -> 
      _([1..8]).map((i) -> util.posMul(v, i) )
    @moveDef = [
      ["1" , [{ x :  1, y : -1, length : 1}]]
      ["2" , [{ x :  0, y : -1, length : 1}]]
      ["3" , [{ x : -1, y : -1, length : 1}]]
      ["4" , [{ x :  1, y :  0, length : 1}]]
      ["6" , [{ x : -1, y :  0, length : 1}]]
      ["7" , [{ x :  1, y :  1, length : 1}]]
      ["8" , [{ x :  0, y :  1, length : 1}]]
      ["9" , [{ x : -1, y :  1, length : 1}]]
      ["a" , [{ x :  1, y : -2, length : 1}]]
      ["b" , [{ x : -1, y : -2, length : 1}]]
      ["1_", create8Aray({ x :  1, y : -1})]
      ["2_", create8Aray({ x :  0, y : -1})]
      ["3_", create8Aray({ x : -1, y : -1})]
      ["4_", create8Aray({ x :  1, y :  0})]
      ["6_", create8Aray({ x : -1, y :  0})]
      ["7_", create8Aray({ x :  1, y :  1})]
      ["8_", create8Aray({ x :  0, y :  1})]
      ["9_", create8Aray({ x : -1, y :  1})]
    ]
    @moveDef1 = {}
    @moveDef2 = {}
    _(@moveDef).each((pair) =>
      @moveDef1[pair[0]] = pair[1]
      @moveDef2[pair[0]] = _(pair[1]).map((v)->util.reverseVector(v, 2))
      )
    @moveKeysShort = {"1":true, "2":true, "3":true, "4":true, "6":true, "7":true, "8":true, "9":true, "a":true, "b":true}
    @moveKeysLong = {"1_":true, "2_":true, "3_":true, "4_":true, "6_":true, "7_":true, "8_":true, "9_":true}

    cutLength = (playerNumber, positions, gameBoard) ->
      ngLength = 0
      ngPos = _(positions).find((pos) -> util.isntVacant(gameBoard.getKoma pos))
      if ngPos
        toKoma = gameBoard.getKoma(ngPos)
        ngLength = if toKoma.playerNumber is playerNumber then ngPos.length else ngPos.length + 1
      return if ngLength isnt 0  then _(positions).filter((pos) -> pos.length < ngLength) else positions

    @getMovablePos = (playerNumber, komaType, position, gameBoard) ->
      moves = komaTypes[komaType].moveDef.moves
      _moveDef = if playerNumber is 1 then @moveDef1 else @moveDef2
      result = []
      _(moves).each((move)-> 
        positions = _(_moveDef[move]).map((v) -> util.posAdd(position, v))
        positions = _(positions).filter((pos)->util.isInRange(pos))
        positions = cutLength(playerNumber, positions, gameBoard)
        result = result.concat(positions)
      )
      result

    @getPutableCell = (playerNumber, komaType, gameBoard) ->
      positions = []
      util.allPos( (x, y) -> 
        if util.isVacant( gameBoard.getKoma({x:x, y:y}) ) 
          positions.push( {x:x,y:y} )
      )
      if komaType is "hu" or komaType is "ky"
        filter = if playerNumber is 1 then (p) -> p.y isnt 0 else (p) -> p.y isnt 8
      if komaType is "ke"
        filter = if playerNumber is 1 then (p) -> p.y > 1 else (p) -> p.y < 7
      if filter then _(positions).filter(filter) else positions

    @getNari = (playerNumber, komaType, fromPos, toPos) ->
      return "none" if komaTypes[komaType].nari is false
      if playerNumber is 1
        if fromPos.y < 3 or toPos.y < 3
          return "force" if ( komaType is "hu" or komaType is "ky" ) and toPos.y is 0
          return "force" if komaType is "ke" and toPos.y < 2
          return "possible"
        else
          return "none"
      else
        if fromPos.y > 5 or toPos.y > 5
          return "force" if ( komaType is "hu" or komaType is "ky" ) and toPos.y is 8
          return "force" if komaType is "ke" and toPos.y > 6
          return "possible"
        else
          return "none"


moveConvertor = new MoveConvertor()

class Koma
  constructor : (@playerNumber, @komaType) ->
  duplicate : -> new Koma(@playerNumber, @komaType)
km1 = (komaType) -> new Koma(1, komaType)
km2 = (komaType) -> new Koma(2, komaType)

class Board
  constructor : () ->
    @komaDai = [[], []]
    @board = [[], [], [], [], [], [], [], [], []]

  clearBoard : -> @board = [[], [], [], [], [], [], [], [], []]
  initBoard : -> 
    @board = [
      [km2('ky'),km2('ke'),km2('gi'),km2('ki'),km2('ou'),km2('ki'),km2('gi'),km2('ke'),km2('ky')] 
      ['',km2('ka'),'','','','','',km2('hi'),'']
      [km2('hu'),km2('hu'),km2('hu'),km2('hu'),km2('hu'),km2('hu'),km2('hu'),km2('hu'),km2('hu')]
      ['','','','','','','','','']
      ['','','','','','','','','']
      ['','','','','','','','','']
      [km1('hu'),km1('hu'),km1('hu'),km1('hu'),km1('hu'),km1('hu'),km1('hu'),km1('hu'),km1('hu')]
      ['',km1('hi'),'','','','','',km1('ka'),'']
      [km1('ky'),km1('ke'),km1('gi'),km1('ki'),km1('ou'),km1('ki'),km1('gi'),km1('ke'),km1('ky')]       
    ]

  duplicate : (playerNumber) ->
    filter = playerNumber is 1 or playerNumber is 2
    board = new Board()
    for y in [0...9]
      for x in [0...9]
        if @board[x][y] and ( not filter or playerNumber is @board[x][y].playerNumber )
          board.board[x][y] = @board[x][y].duplicate()
        else
          board.board[x][y] = ''

    if not filter or playerNumber is 1
      board.komaDai[0] = board.komaDai[0].concat(@komaDai[0])
    if not filter or playerNumber is 2
      board.komaDai[1] = board.komaDai[1].concat(@komaDai[1])
    return board

  getKoma : (position) -> 
    try
      @board[position.y][position.x]
    catch e
      throw e

  getExistsHuColumn : (playerNumber) ->
    result = {}
    for y in [0...9]
      for x in [0...9]
        koma = @board[y][x]
        if koma and koma.playerNumber is playerNumber and koma.komaType is "hu"
          result[x] = true
    return result



  putKoma : (koma, position) =>
    @board[position.y][position.x] = koma

  removeKoma : (position) =>
    @board[position.y][position.x] = undefined

  getKomaFromKomadai : (playerNumber, komaType) ->
    find = false
    index = if playerNumber is 1 then 0 else 1
    koma = _(@komaDai[index]).find((koma) -> koma.komaType is komaType)
    @komaDai[index] = _(@komaDai[index]).filter((koma) -> 
      return true if find
      return !(find = true) if koma.komaType is komaType
      return true)
    return koma

  putKomaToKomadai : (playerNumber, koma) ->
    koma = koma.duplicate()
    koma.playerNumber = playerNumber
    type = komaTypes[koma.komaType]
    koma.komaType = komaTypes[koma.komaType].resourceId
    if playerNumber is 1
      @komaDai[0].push koma
      @komaDai[0] = _(@komaDai[0]).sortBy((koma)->komaTypes[koma.komaType].value)
    else
      @komaDai[1].push koma
      @komaDai[1] = _(@komaDai[1]).sortBy((koma)->komaTypes[koma.komaType].value)

  getKomaWithPosition : (predicate) =>
    result = []
    for y in [0...9]
      for x in [0...9]
        koma = @board[y][x]
        if koma and predicate(koma) 
          result.push({koma : koma, position : {x:x,y:y} })
    result

  getPlayerKoma : (playerNumber) =>
    @getKomaWithPosition( (koma) -> koma.playerNumber is playerNumber )

  getPlayerKomaFromKomadai : (playerNumber) => if playerNumber is 1 then @komaDai[0] else @komaDai[1]


  getOu : (playerNumber) =>
    ou = @getKomaWithPosition( (koma) -> koma.playerNumber is playerNumber and koma.komaType is "ou" )
    return if ou[0] then ou[0] else null

class Game
  constructor : (player1, player2, custom) ->
    @board = new Board()
    @board.initBoard()
    @turn  = 1
    @player1info = player1
    @player2info = player2
    #@player1 = { life: 9, time : 600000 }
    #@player2 = { life: 9, time : 600000 }
    @player1 = { life: 9, time : 600000 }
    @player2 = { life: 9, time : 600000 }
    if custom
      if custom.teai is 'teai'
        @player1.life = custom.player1.life
        @player1.time = custom.player1.time * 60000
        @player2.life = custom.player2.life
        @player2.time = custom.player2.time * 60000
      else
        @player1.life = custom.player1.life
        @player1.time = custom.player1.time * 60000
        @player2.life = custom.player1.life
        @player2.time = custom.player1.time * 60000

      @custom = custom

    @player1.life = 99 if @player1.life > 99
    @player1.life = 0 if @player1.life <= 0
    @player2.life = 99 if @player2.life > 99
    @player2.life = 0 if @player2.life <= 0

    @player1.time = 60000 if @player1.time <= 60000
    @player2.time = 60000 if @player2.time <= 60000



    @state = "wating"
    @judge = new SyogiJudgement()
    @kifu = []
    @winPlayerNumber = 0
    @sendMessage = ->
    @messageCache = { 0 : [], 1 : [], 2 : [] }
    @stateCache = {
      oute : false,
      getKoma : false,
      emphasisPosition : null
    }
    @moveInfoCache = ""

  getPlayerAccountInfo : =>
    return {
      player1 : @player1info
      player2 : @player2info
    }

  getPlayerInfoCommand : =>
    return {
      method : "adjustTime"
      player1 : @player1
      player2 : @player2
    }
  getPlayerInfo : =>
    return {
      player1 : @player1
      player2 : @player2
    }
  duplicatePlayerInfo : =>
    return {
      player1 : { "life" : @player1.life, "time" : @player1.time }
      player2 : { "life" : @player2.life, "time" : @player2.time }
    }

  getKifu : => 
    kifu : @kifu
    account : @getPlayerAccountInfo()
    playerInfo : @getPlayerInfo()
    custom : @custom

  getElapsedTime : => Date.now() - @currentTime

  startGame : => 
    @currentTime = Date.now()
    @startDateTime = new Date()
    @state = "playing"
    @sendMessages([messages.game_start, messages.turn_own], [messages.game_start, messages.turn_opp])

  timeout : (playerNumber) =>
    end = @getCurrentPlayer().time + @currentTime - Date.now() < 0 and @state isnt "end"
    if end 
      @state = "end"
      @sendMessages([ messages.win_timeout_opp ], [ messages.win_timeout_own ], true)
      @sendMessagesBroadCast( (if ( (2-@turn%2) is 1 ) then [messages.win_timeout_p2] else [messages.win_timeout_p1] ), true)
      playerInfo = @duplicatePlayerInfo()
      if (2-@turn%2) is 1
        playerInfo.player1.time = 0
      else
        playerInfo.player2.time = 0
      @kifu.push { playerInfo : playerInfo, turn : @turn, foul : false, info : @createEndKifuInfo( @turn%2 + 1, "timeout") }
      @distributeKifu @kifu[@kifu.length-1]
    return end

  resign : (playerNumber) =>
    end = @state isnt "end"
    if end
      @state = "end"
      @sendMessage( playerNumber   , "modal", [ messages.win_resign_opp ]  )
      @sendMessage( 3-playerNumber , "modal", [ messages.win_resign_own ]  )
      @sendMessagesBroadCast( (if ( playerNumber is 1 ) then [messages.win_resign_p2] else [messages.win_resign_p1] ), true)
      @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn, foul : false, info : @createEndKifuInfo( 3-playerNumber, "resign") }
      @distributeKifu @kifu[@kifu.length-1]
    return end


  disconnectPlayer : (playerNumber) =>
    console.log ("disconnect " + playerNumber )
    if playerNumber is 2-@turn%2
      @sendMessages([ messages.win_discconect_opp ], [ messages.win_discconect_own ], true)
    else 
      @sendMessages([ messages.win_discconect_own ], [ messages.win_discconect_opp ], true)
    @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn, foul : false, info : @createEndKifuInfo( 3-playerNumber, "disconnect") }
    @sendMessagesBroadCast( (if ( playerNumber is 1 ) then [messages.win_discconect_p1] else [messages.win_discconect_p2] ), true)

  nextTurn : =>
    oldTime = @currentTime
    @currentTime = Date.now()
    timeSpan = @currentTime - oldTime
    player = @getCurrentPlayer()
    console.log("player.time : " + player.time)
    console.log("timeSpan : " + timeSpan)
    player.time -= timeSpan
    console.log("残り時間 : " + player.time)
    @turn++

  setMessageSender : (sendMessage) => @sendMessage = sendMessage
  setKifuDistributor : (distributeKifu) => @distributeKifu = distributeKifu

  sendMessages : (ownMessage, oppMessage, modal) =>
    type = if modal then "modal" else "plain"
    @sendMessage( @getOwnPlayerNum(), type, ownMessage  )
    @sendMessage( @getOppPlayerNum(), type, oppMessage  )
    @messageCache = { 0 : [] }
    @messageCache[@getOwnPlayerNum()] = { type : type, message : ownMessage }
    @messageCache[@getOppPlayerNum()] = { type : type, message : oppMessage } 
  sendMessagesBroadCast : (message, modal) =>
    type = if modal then "modal" else "plain"
    @sendMessage( 3, type, message  )

  foul : (moveInfo, bkBoard) =>
    
    @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn, foul : true, info : @createKifuInfo(moveInfo) } 
    @distributeKifu @kifu[@kifu.length-1]
    @getCurrentPlayer().life--
    end = @getCurrentPlayer().life < 0
    if end
      @state = "end"
      @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn, foul : false, info : @createEndKifuInfo(2-(@turn+1)%2, "foul") }
      @distributeKifu @kifu[@kifu.length-1]
      @sendMessages([ messages.win_foul_opp ], [ messages.win_foul_own ], true)
      @sendMessagesBroadCast( (if ( (2-(@turn%2) ) is 1 ) then [messages.win_foul_p2] else [messages.win_foul_p1] ), true)
    else
      @sendMessages([ messages.foul_own ], [ messages.foul_opp ])

    @board = bkBoard
    return {
      ownHistory : [ method : "foul", playerNumber : 2-@turn%2 ]
      oppHistory : [ method : "foul", playerNumber : 2-@turn%2 ]
      end        : end
    }

  getPlayerBord : (playerNumber) =>
    return { 
    playerNumber : playerNumber,
    account : @getPlayerAccountInfo()
    board : @board.duplicate(playerNumber), 
    turn : @turn, 
    playerInfo : @getPlayerInfo(),
    message : { type : @messageCache[playerNumber].type, texts : @messageCache[playerNumber].message },
    elapsedTime : @getElapsedTime()
    state : @stateCache }

  moveKoma : (moveInfo, rollBack) =>
    ownHistory = []
    oppHistory = []
    ownMessage = []
    oppMessage = []
    bkBoard = @board.duplicate()
    oute = false
    getKoma = false
    emphasisPosition = null

    canMoveResult = @judge.canMove(@, 2-@turn%2 , moveInfo)
    if canMoveResult is "NG"
      if not rollBack
        foulCommand = @foul(moveInfo, bkBoard)
        ownHistory = foulCommand.ownHistory
        oppHistory = foulCommand.oppHistory
        end        = foulCommand.end
        return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : end }
    else if canMoveResult is "Contradiction"
        ownHistory = [ method : "contradiction"]
        return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : false, contradiction : true }

    fromKoma = @board.getKoma moveInfo.from.position
    toKoma = @board.getKoma moveInfo.to.position
    if util.isntVacant toKoma
      @board.putKomaToKomadai(2-@turn%2, toKoma)
      ownHistory.push { method : "putKomaToKomadai", playerNumber : 2-@turn%2, koma : toKoma.duplicate() }
      oppHistory.push { method : "removeKoma", playerNumber : 2-(@turn-1)%2, position : moveInfo.to.position }
      getKoma = true
      emphasisPosition = moveInfo.to.position
      if not rollBack
        ownMessage.push ( getKomaName(toKoma.komaType) + messages.get_koma_own )
        oppMessage.push ( komaTypes[toKoma.komaType].name + messages.get_koma_opp )
        ownHistory.push { method : "noticeGetKoma", getKoma : true }
        oppHistory.push { method : "noticeGetKoma", getKoma : true }

    @board.removeKoma moveInfo.from.position
    ownHistory.push { method : "removeKoma", playerNumber : 2-@turn%2, position : moveInfo.from.position }
    
    putKoma = fromKoma.duplicate()
    putKoma.komaType = moveInfo.to.komaType
    @board.putKoma(putKoma, moveInfo.to.position)
    toKoma = fromKoma.duplicate()
    toKoma.komaType = moveInfo.to.komaType
    ownHistory.push { method : "putKoma", playerNumber : 2-@turn%2, koma : toKoma, position : moveInfo.to.position }

    if @judge.isOute(@, 2-@turn%2)
      if not rollBack
        foulCommand = @foul(moveInfo, bkBoard)
        ownHistory = foulCommand.ownHistory
        oppHistory = foulCommand.oppHistory
        end        = foulCommand.end
      @board = bkBoard
      return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : end }
    if rollBack
      @board = bkBoard
    else
      @turn++
      tsumi = @judge.isTsumi(@, 2-@turn%2)
      @turn--
      oute = false
      if tsumi
        @sendMessages([messages.win_own], [messages.win_opp], true)
        @sendMessagesBroadCast( (if ( (2-@turn%2) is 1 ) then [messages.win_p1] else [messages.win_p2] ), true)
      else
        if @judge.isOute(@, @getOppPlayerNum() )
          oute = true
          ownMessage.push messages.oute_own
          oppMessage.push messages.oute_opp
        ownHistory.push { method : "noticeOute", oute : oute }
        oppHistory.push { method : "noticeOute", oute : oute }
        ownMessage.push messages.turn_opp
        oppMessage.push messages.turn_own
        @sendMessages(ownMessage, oppMessage)
      
      @nextTurn()
      @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn-1, foul : false, info : @createKifuInfo(moveInfo) }
      @distributeKifu @kifu[@kifu.length-1]
      if tsumi
        @state = "end"
        @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn-1, foul : false, info : @createEndKifuInfo(2-(@turn-1)%2, "tsumi") }
        @distributeKifu @kifu[@kifu.length-1]
      ownHistory.push @getPlayerInfoCommand()
      oppHistory.push @getPlayerInfoCommand()

    if not rollBack
      @stateCache = {
        oute : oute,
        getKoma : getKoma,
        emphasisPosition : emphasisPosition
      }
    return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : true, tsumi : tsumi, oute : oute }

  getCurrentPlayer : => if @turn%2 is 1 then @player1 else @player2
  getOwnPlayerNum : => if @turn%2 is 1 then 1 else 2
  getOppPlayerNum : => if @turn%2 is 1 then 2 else 1

  isTurn : (playerNumber) => if playerNumber is (2-@turn%2) then true else false

  canMoveKoma : (moveInfo, playerNumber) => @moveKoma(moveInfo, true).allowed

  putKoma : (moveInfo, rollBack) =>
    ownHistory = []
    oppHistory = []
    ownMessage = []
    oppMessage = []
    bkBoard    = @board.duplicate()
    fromKoma   = @board.getKomaFromKomadai( 2-@turn%2, moveInfo.to.komaType )

    if not fromKoma
      @board = bkBoard
      ownHistory = [ method : "contradiction" ]
      return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : false, contradiction : true }

    toKoma = @board.getKoma moveInfo.to.position
    if util.isntVacant toKoma
      if not rollBack
        foulCommand = @foul(moveInfo, bkBoard)
        ownHistory = foulCommand.ownHistory
        oppHistory = foulCommand.oppHistory
        end        = foulCommand.end
      @board = bkBoard
      return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : end }

    
    if moveInfo.to.komaType is "hu"
      columns = @board.getExistsHuColumn(2-@turn%2)
      if columns[moveInfo.to.position.x]
        if not rollBack
          foulCommand = @foul(moveInfo, bkBoard)
          ownHistory = foulCommand.ownHistory
          oppHistory = foulCommand.oppHistory
          end        = foulCommand.end
        @board = bkBoard
        return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : end }

    @board.putKoma(fromKoma, moveInfo.to.position)
    ownHistory.push { method : "removeKomaFromKomadai", playerNumber : 2-@turn%2, koma : fromKoma.duplicate() }
    ownHistory.push { method : "putKoma", playerNumber : 2-@turn%2, koma : fromKoma.duplicate(), position : moveInfo.to.position }

    if @judge.isOute(@, 2-@turn%2)
      if not rollBack
        foulCommand = @foul(moveInfo, bkBoard)
        ownHistory = foulCommand.ownHistory
        oppHistory = foulCommand.oppHistory
        end        = foulCommand.end
      @board = bkBoard
      return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : false, end : end }
    if not rollBack
      @turn++
      tsumi = @judge.isTsumi(@, 2-@turn%2)
      @turn--
      if tsumi and moveInfo.to.komaType is "hu"
          foulCommand = @foul(moveInfo, bkBoard)
          ownHistory = foulCommand.ownHistory
          oppHistory = foulCommand.oppHistory
          end        = foulCommand.end
          return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : false, tsumi : tsumi, end : end }
      else
        if tsumi
          @sendMessages([messages.win_own], [messages.win_opp], true)
          @sendMessagesBroadCast( (if ( (2-@turn%2) is 1 ) then [messages.win_p1] else [messages.win_p2] ), true)
        else
          oute = false
          if @judge.isOute(@, @getOppPlayerNum() )
            oute = true
            ownMessage.push messages.oute_own
            oppMessage.push messages.oute_opp
          ownHistory.push { method : "noticeOute", oute : oute }
          oppHistory.push { method : "noticeOute", oute : oute }
          ownMessage.push messages.turn_opp
          oppMessage.push messages.turn_own
          @sendMessages(ownMessage, oppMessage)
        
        @nextTurn()
        @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn-1, foul : false, info : @createKifuInfo(moveInfo) }
        @distributeKifu @kifu[@kifu.length-1]
        if tsumi
          @state = "end"
          @kifu.push { playerInfo : @duplicatePlayerInfo(), turn : @turn-1, foul : false, info : @createEndKifuInfo(2-(@turn-1)%2, "tsumi") }
          @distributeKifu @kifu[@kifu.length-1]
        ownHistory.push @getPlayerInfoCommand()
        oppHistory.push @getPlayerInfoCommand()
    if rollBack
      @board = bkBoard

    if not rollBack
      @stateCache = {
        oute : oute,
        getKoma : false,
        emphasisPosition : null
      }
    return { ownHistory : ownHistory, oppHistory : oppHistory, allowed : true, tsumi : tsumi, oute : oute }

  canPutKoma : (moveInfo) => @putKoma(moveInfo, true).allowed

  isTsumi : => 
    if @judge.isTsumi(@, 2-@turn%2)
      @sendMessages( [messages.win_opp] , [messages.win_own], true)
      return true
    return false

  createKifuInfo : (moveInfo) =>
    return { 
      type : "moveKoma"
      from : if moveInfo.from then moveInfo.from.position else { x : -1 , y : -1}
      to   : moveInfo.to.position
      koma : moveInfo.to.komaType
    }

  createEndKifuInfo : (winPlayerNumber, reason) =>
    @winPlayerNumber = winPlayerNumber
    return { 
      type            : "endGame"
      winPlayerNumber : winPlayerNumber
      reason          : reason
    }

  showKifu : => _(@kifu).map( (te) -> "#{te.turn} : #{te.foul} : " + (te.info.from.x+1) + (te.info.from.y+1) + (te.info.to.x+1) + (te.info.to.y+1) + te.info.koma )


class SyogiJudgement
  constructor : () ->

  canMove : (game, playerNumber, moveInfo) ->
    return "Contradiction" if game.state isnt "playing"
    return "Contradiction" if game.turn % 2 isnt playerNumber % 2
    fromKoma = game.board.getKoma(moveInfo.from.position)
    toKomaType = komaTypes[moveInfo.to.komaType]
    return "Contradiction" if not util.isInRange(moveInfo.from.position) or not util.isInRange(moveInfo.to.position)
    return "Contradiction" if !fromKoma
    return "Contradiction" if fromKoma.komaType isnt moveInfo.from.komaType
    return "Contradiction" if toKomaType.id isnt fromKoma.komaType and toKomaType.resourceId isnt fromKoma.komaType
    toKoma   = game.board.getKoma(moveInfo.to.position)
    return "Contradiction" if toKoma and toKoma.playerNumber is playerNumber
    pos = _(moveConvertor.getMovablePos(playerNumber, moveInfo.from.komaType, moveInfo.from.position, game.board)).find((pos)->util.posEq(pos, moveInfo.to.position))
    return "OK" if pos
    return "NG"

  isOute : (game, playerNumber) =>
    kikiMap = {}
    enemyPlayerNumber = if playerNumber is 1 then 2 else 1
    _( game.board.getPlayerKoma( enemyPlayerNumber ) ).each( (komaInfo) ->
      postions = _(moveConvertor.getMovablePos(enemyPlayerNumber, komaInfo.koma.komaType, komaInfo.position, game.board))
      postions = _(postions).map((pos)-> util.posToStr(pos))
      _( postions ).each( (p) -> kikiMap[p] = true )
    )
    ou = game.board.getOu playerNumber
    ouPos = util.posToStr ou.position
    return if kikiMap[ouPos] then true else false

  isTsumi : (game, playerNumber) =>
    _cache = ""
    return ( not _( game.board.getPlayerKoma( playerNumber ) ).some( (komaInfo) ->
      positions = moveConvertor.getMovablePos(playerNumber, komaInfo.koma.komaType, komaInfo.position, game.board)
      return _(positions).some( (pos) -> 
        moveInfo = 
          from : 
            position : komaInfo.position
            komaType : komaInfo.koma.komaType
          to   :
            position : pos
            komaType : komaInfo.koma.komaType
        return game.canMoveKoma moveInfo
      )
    ) ) and ( not _( game.board.getPlayerKomaFromKomadai( playerNumber ) ).some( (koma) ->
      return false if _cache is koma.komaType
      _cache = koma.komaType
      positions = moveConvertor.getPutableCell(playerNumber, koma.komaType, game.board)
      return _(positions).some( (pos) -> 
        moveInfo = 
          to   :
            position : pos
            komaType : koma.komaType
        return game.canPutKoma moveInfo
      )
    ) )

module.exports = Game