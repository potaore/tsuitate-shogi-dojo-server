import Artifact = require('../../potaore/artifact');
import base = require('./Base');
import img = require('./ImageManager');
import ai = require('./AI')
declare let $: any;

export class BoardCanvas extends Artifact.Artifact {
  canvas;
  ctx;
  mouseX;
  mouseY;
  eventEffective: boolean = true;
  constructor(canvas) {
    super('board-canvas');
    this.canvas = canvas;
    this.canvas.get(0).width = 640;
    this.canvas.get(0).height = 640;
  }

  init() {
    this.ctx = this.canvas.get(0).getContext('2d');
    this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
    this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
  }
  
  putKoma(image, physicalPosition) {
    this.ctx = this.canvas.get(0).getContext('2d');
    this.ctx.globalAlpha = 1;
    this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
  }

  putKomaAlpha(image, physicalPosition, alpha) {
    this.ctx = this.canvas.get(0).getContext('2d');
    this.ctx.globalAlpha = alpha;
    this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
  }

  checkCell(position) {
    this.ctx.fillStyle = 'rgba(256, 150, 150, 0.5)';
    this.ctx.fillRect(5 + position.x * 70, 5 + position.y * 70, 70, 70);
  }
}


export class KomadaiCanvas extends Artifact.Artifact {
  num: number;
  canvas;
  ctx;
  mouseX;
  mouseY;
  rects = [];
  imageManager: img.ImageManager;
  emphasizeKoma;
  eventEffective: boolean = false;
  constructor(canvas, num: number, imageManager: img.ImageManager) {
    super('komadai');
    this.num = num;
    this.canvas = canvas;
    this.imageManager = imageManager;

    this.canvas.get(0).width = 310;
    this.canvas.get(0).height = 64;
  }

  init() {
    this.ctx = this.canvas.get(0).getContext('2d');
    this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
    this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
  }

  drawKoma(komalist, flip) {
    this.init();
    let tmp = '';
    let list = null
    let komaTypeList = []
    komalist.forEach(koma => {
      if (tmp != koma.komaType) {
        if (list != null) komaTypeList.push(list);
        list = [koma];
      } else {
        list.push(koma);
      }
      tmp = koma.komaType;
    });
    if (list != null) komaTypeList.push(list);

    let typeCount = 0;
    let ctx = this.ctx;
    this.rects = [];
    let rects = this.rects;
    komaTypeList.forEach(list => {
      let x = 10 + typeCount * 42;
      let y = 10;
      ctx.drawImage(this.imageManager.getKomaImage(list[0], flip), x, y, 45, 45);
      let rect = { koma: list[0], x: x, width: 42, y: y, height: 45 };
      rects.push(rect);

      if (this.emphasizeKoma) {
        if (this.emphasizeKoma.komaType == list[0].komaType) {
          this.emphasizeRect(rect);
          this.emphasizeKoma = null;
        }
      }

      if (list.length > 1) {
        this.circle(25 + typeCount * 42, 10, 7);
        this.ctx.fillStyle = 'rgba(255, 255, 255, 0.99)';
        this.ctx.font = 'bold 15px "Arial"';
        this.ctx.textAlign = 'center';
        this.ctx.fillText(list.length, 25 + typeCount * 42, 15);
      }

      typeCount++;
    });
  }

  circle(x, y, r) {
    let ctx = this.canvas.get(0).getContext('2d');
    ctx.beginPath();
    ctx.arc(x, y, r, 0, 2 * Math.PI, false);
    ctx.fillStyle = '#0275d8';
    ctx.fill();
    ctx.lineWidth = 5;
    ctx.strokeStyle = '#0275d8';
    ctx.stroke();
  }

  checkRect(rect) {
    this.ctx.lineWidth = 5;
    this.ctx.strokeStyle = 'rgba(256, 150, 150, 0.5)';
    this.ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
  }

  emphasizeRect(rect) {
    this.ctx.lineWidth = 5;
    this.ctx.font = 'bold 15px "Arial"';
    this.ctx.strokeStyle = 'rgba(0, 255, 255, 0.8)';
    this.ctx.strokeRect(rect.x + 2, rect.y, rect.width, rect.height);
  }

  emphasize(koma) {
    this.emphasizeKoma = koma;
  }
}

export class BoardGraphicManager extends Artifact.Artifact {
  boardCanvas: BoardCanvas;
  komadai1Canvas: KomadaiCanvas;
  komadai2Canvas: KomadaiCanvas;
  params = { flip: false, oute: false, blind: 0 };
  memo: { game: base.Game } = { game: null };
  playerInfoMemo = null;
  gameInfoMemo = null;
  invatedPosition: { x: number, y: number };
  imageManager: img.ImageManager;
  moveToPositions: any[] = [];
  putToPositions: any[] = [];
  putKomaType: any;
  fromPosition: { x: number, y: number };
  constructor(board, komadai1, komadai2, imageManager: img.ImageManager) {
    super('BoardGraphicManager');
    this.boardCanvas = new BoardCanvas(board);
    this.komadai1Canvas = new KomadaiCanvas(komadai1, 1, imageManager);
    this.komadai2Canvas = new KomadaiCanvas(komadai2, 2, imageManager);
    this.imageManager = imageManager;
    this.addEvents();

    this.listen(this.boardCanvas)
      .listen(this.komadai1Canvas)
      .listen(this.komadai2Canvas);
  }

  addEvents() {
    this.on('draw-clear', (eventName, sender, predictionMap) => this.clearKoma());
    this.on('draw-init', (eventName, sender, predictionMap) => this.init());
    this.on('draw-koma', (eventName, sender, game) => this.drawKoma(game));
    this.on('draw-komadai', (eventName, sender, game) => this.drawKomaDai(game));
    this.on('draw-prediction', (eventName, sender, predictionMap) => this.drawPrediction(predictionMap));
  }

  init() {
    this.params = { flip: false, oute: false, blind: 0 };
    this.memo = { game: null };
  }

  drawKomaDai(arg) {
    let komadai1 = this.params.flip ? this.komadai2Canvas : this.komadai1Canvas;
    let komadai2 = this.params.flip ? this.komadai1Canvas : this.komadai2Canvas;
    komadai1.drawKoma(arg.board.komaDai[0], this.params.flip);
    komadai2.drawKoma(arg.board.komaDai[1], this.params.flip);
  }
  
  drawKoma(game : base.Game) {
    base.util.allPos((x, y) => {
      let koma = game.board.getKoma({x: x, y : y});
      if(koma) {
        let komaimg = this.imageManager.getKomaImage(koma, false);
        this.boardCanvas.putKoma(komaimg, { x: x, y: y });
      }

    });
  }

  drawPrediction(predictionMap: ai.PredictionMap) {
    base.util.allPos((x, y) => {
      let koma = this.imageManager.getKomaImage({ playerNumber: 3 - predictionMap.game.playerNumber, komaType: predictionMap.komaType }, false);
      this.boardCanvas.putKomaAlpha(koma, { x: x, y: y }, predictionMap.board[y][x]);
    });
  }

  putKoma(pos) {
    let moveInfo = {
      to: {
        position: pos,
        komaType: this.putKomaType
      }
    }
  }

  clearKoma() {
    this.boardCanvas.init();
    this.komadai1Canvas.init();
    this.komadai2Canvas.init();
  }

  initializeTableState() {
    this.moveToPositions = [];
    this.fromPosition = null;
    this.putToPositions = [];
    this.putKomaType = '';
    this.boardCanvas.init()
  }

  setPlayerInfo(command) {
    this.playerInfoMemo = command.account;
    domFinder.getPlayerImageDiv(1, this.params.flip).empty();
    domFinder.getPlayerImageDiv(2, this.params.flip).empty();
    domFinder.getPlayerNameDiv(1, this.params.flip).empty();
    domFinder.getPlayerNameDiv(2, this.params.flip).empty();
    domFinder.getPlayerImageDiv(1, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player1.profile_url, this.playerInfoMemo.player1.character));
    domFinder.getPlayerNameDiv(1, this.params.flip).append(this.playerInfoMemo.player1.name);
    domFinder.getPlayerImageDiv(2, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player2.profile_url, this.playerInfoMemo.player2.character));
    domFinder.getPlayerNameDiv(2, this.params.flip).append(this.playerInfoMemo.player2.name);
  }

  setTurn(turn) {
    domFinder.getGameTurnDiv().empty();
    if (turn) {
      domFinder.getGameTurnDiv().append(turn + '手');
    } else {
      domFinder.getGameTurnDiv().append('対局開始');
    }

    let playerTurn = turn % 2;
    domFinder.getPlayerNameDiv(1 + playerTurn, this.params.flip).addClass('label-success');
    domFinder.getPlayerNameDiv(playerTurn, this.params.flip).removeClass('label-success');
  }

  updateGameInfo(command) {
    let emphasize1 = false;
    let emphasize2 = false;
    if (this.gameInfoMemo) {
      emphasize1 = command.player1.life < this.gameInfoMemo.player1.life;
      emphasize2 = command.player2.life < this.gameInfoMemo.player2.life;
    }
    this.gameInfoMemo = {
      player1: {
        time: command.player1.time,
        life: command.player1.life
      },
      player2: {
        time: command.player2.time,
        life: command.player2.life
      }
    };
    domFinder.getPlayerTimeDiv(1, this.params.flip).empty();
    domFinder.getPlayerTimeDiv(2, this.params.flip).empty();
    domFinder.getPlayerLifeLabel(1, this.params.flip).empty();
    domFinder.getPlayerLifeLabel(2, this.params.flip).empty();
    domFinder.getPlayerTimeDiv(1, this.params.flip).append(this.computeDuration(command.player1.time));
    domFinder.getPlayerTimeDiv(2, this.params.flip).append(this.computeDuration(command.player2.time));
    domFinder.getPlayerLifeLabel(1, this.params.flip).append(command.player1.life);
    domFinder.getPlayerLifeLabel(2, this.params.flip).append(command.player2.life);
  }

  resetGameInfo() {
    if (this.gameInfoMemo) this.updateGameInfo(this.gameInfoMemo);
  }

  computeDuration(ms) {
    if (ms < 0) ms = 0;
    let h = parseInt(new String(Math.floor(ms / 3600000) + 100).substring(1));
    let m = parseInt(new String(Math.floor((ms - h * 3600000) / 60000) + 100).substring(1));
    let s = new String(Math.floor((ms - h * 3600000 - m * 60000) / 1000) + 100).substring(1);
    return m + ':' + s;
  }
}

let domFinder = {
  getPlayerNameDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerImageDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getGameTurnDiv: function () {
    return $('#game-turn-label');
  },
  getPlayerTimeDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerLifeLabel: function (playerNumber, flip) {
    if (flip) {
      return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getPlayerLifeDiv: function (playerNumber, flip) {
    if (flip) {
      return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
    } else {
      return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
    }
  },
  getFlipItem: function () {
    return $('.flip-item');
  },
  getNotFlipItem: function () {
    return $('.not-flip-item');
  }
};