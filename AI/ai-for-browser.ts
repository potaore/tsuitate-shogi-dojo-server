
declare let $: any;
import artifact = require('../../potaore/artifact');
import base = require('./Base');
import ai = require('./AI');
import bgraph = require('./BoardGraphic');
import img = require('./ImageManager');


$(document).ready(() => {
  let bot = new ai.BotClient();
  let imageManager = new img.ImageManager();
  let boardGraphic = new bgraph.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager);

  boardGraphic.listen(bot);
  bot.start();
});
