declare function require(x: string): any;
import Artifact = require('../../potaore/artifact');
import Base = require('./Base');

let getRandom = (array: any[], predicate) => {
  if (array.length == 0) return;
  predicate(array[Math.floor(array.length * Math.random())]);
};

interface MoveInfo {
  from: {
    position: any,
    komaType: any
  }
  to: {
    position: any,
    komaType: any
  }
}

class GameState {
  oute: boolean;
  foul: boolean;
  getKoma: boolean;
}

export class BotClient extends Artifact.Artifact {
  socket: any;
  game: Base.Game;
  board: Base.Board;
  state: GameState = new GameState();
  moveConvertor = new Base.MoveConvertor();
  strategy: Strategy;
  constructor() {
    super('bot-client');
  }

  start() {
    var socket = require('socket.io-client')('http://133.130.72.92');
    //var socket = require('socket.io-client')('http://localhost:8080');
    socket.on('connect', function () { });
    socket.on('event', function (data) { });
    socket.on('disconnect', function () { });
    socket.on('noticePlayerNumber', (arg) => {
      this.startGame(arg);
      this.executeIfMyTurn();
    });

    socket.on('command', (command) => {
      this.updateGame(command);
      this.executeIfMyTurn();
    });
    socket.on('synchronize', (command) => {
    });
    socket.on('endGame', (arg) => {
      this.next(() => {
        if (arg.kifu[arg.kifu.length - 1].info.winPlayerNumber == this.game.playerNumber) {
          socket.emit('message', 'あたしの勝ちね！');
        } else {
          socket.emit('message', 'まけちゃった。。。');
        }
        this.next(() => {
          socket.emit('room', { method: 'exitRoom' });
          this.next(() => {
            socket.emit('room', { method: 'cancelGame' });
            this.next(() => {
              socket.emit('room', { method: 'startGame', type: 'bot' });
            });
          });
        });
      });
    });

    socket.emit('auth.guestlogin.bot', { name: 'Regina (lv2.2)', character: 54 });
    socket.emit('room', { method: 'startGame', type: 'bot' });

    this.socket = socket;
  }

  startGame(arg) {
    this.game = new Base.Game(arg.playerNumber, arg.playerInfo.player1, arg.playerInfo.player2, this.socket);
    this.game.start();
    this.state = new GameState();
    if (this.strategy) this.strategy.dispose();
    this.strategy = new Strategy(this.game, this.moveConvertor, this.state);
  }

  updateGame(commands: any[]) {
    this.state.foul = false;
    let getKomaFlag = false;
    let outeFlag = false;
    let contradiction = false;
    let removePosition = null;
    let komaType = null;
    commands.forEach((command) => {
      if (command.method == 'removeKomaFromKomadai') {
        this.game.board.removeKomaFromKomadai(command.playerNumber, command.koma.komaType);
      } else if (command.method == 'putKomaToKomadai') {
        this.game.board.putKomaToKomadai(command.playerNumber, command.koma);
        komaType = command.koma.komaType;
      } else if (command.method == 'removeKoma') {
        this.game.board.removeKoma(command.position);
        this.strategy.getKoma(command.position);
      } else if (command.method == 'putKoma') {
        this.game.board.putKoma(command.koma, command.position);
        removePosition = command.position;
      } else if (command.method == 'adjustTime') {
        this.game.board.player1.time = command.player1.time;
        this.game.board.player2.time = command.player2.time;
      } else if (command.method == 'foul') {
        this.game.getCurrentPlayer().life--;
        this.state.foul = true;
        this.strategy.foul();
      } else if (command.method == 'noticeOute') {
        outeFlag = command.oute;
      } else if (command.method == 'noticeGetKoma') {
        getKomaFlag = command.getKoma;
      } else if (command.method == 'contradiction') {
        contradiction = true;
      }
    });

    if (this.game.isPlayerTurn() && getKomaFlag) {
      this.strategy.prediction.converge(komaType, removePosition);
      this.strategy.prediction.setKomadai(komaType);
    } else {

    }

    if (!contradiction) {
      if (this.state.foul) {
      } else {
        this.state.getKoma = getKomaFlag;
        this.state.oute = outeFlag;
        this.game.nextTurn();
        this.strategy.next();
        if (this.state.oute) {
          this.strategy.oute();
        }

        //console.log(JSON.stringify(this.strategy.predictionMap.board));
        //console.log(JSON.stringify(this.strategy.suppressionMap.booleanBoard));
      }
    } else {
      this.socket.emit('game', { method: 'synchronize' });
    }
  }

  executeIfMyTurn() {
    if (this.game.isPlayerTurn()) {
      this.next(() => this.execute());
    }
    this.notify('draw-clear');
    this.notify('draw-koma', this.game);
    this.notify('draw-komadai', this.game);

    this.strategy.prediction.predictionMaps.forEach(map => {
      this.notify('draw-prediction', map);
    });
  }

  execute() {
    let moveInfo = this.strategy.execute();
    let action = moveInfo.from.position.x == -1 && moveInfo.from.position.y == -1 ? 'putKoma' : 'moveKoma';
    this.socket.emit('game', {
      method: action,
      args: moveInfo
    });
  }

  next(predicate) {
    setTimeout(function () {
      predicate();
    }, 500);
  }
}

export class Strategy {
  game: Base.Game;
  isKomaHitted: boolean = false;
  komaHittedCount: number = 0;
  foulHands: {} = {};
  tryPositions: any[] = [];
  moveInfoCache: MoveInfo;
  prevMoveInfo: MoveInfo;
  moveConvertor: Base.MoveConvertor;
  state: GameState;
  invatedPosition: { x: number, y: number };
  predictionMap_ou: PredictionMap;
  predictionMap_hu: PredictionMap[] = [null, null, null, null, null, null, null, null, null];
  prediction: Prediction;
  moveHisyaInOpening: boolean = false;
  moveKakuInOpening: boolean = false;
  moveHuForKakuInOpening: boolean = false;
  prevGetKoma: boolean = false;
  useKakuCountDown : number = 2;

  //駒取り返しを企むフラグ
  recapture: boolean = false;
  recapturePosition: { x: number, y: number };
  //王手を一度でもかけたフラグ
  getOute: boolean = false;
  suppressionMap: SuppressionMap = new SuppressionMap();
  invatedMap: SuppressionMap = new SuppressionMap();
  foulCount: number = 0;

  constructor(game: Base.Game, moveConvertor: Base.MoveConvertor, state: GameState) {
    this.game = game;
    this.moveConvertor = moveConvertor;
    this.state = state;

    this.prediction = new Prediction(moveConvertor, game, state, this);

    let dummyGame = new Base.Game(3 - this.game.playerNumber, {}, {}, {});
    Base.util.allPos((x, y) => {
      let koma = dummyGame.board.getKoma({ x, y });
      if (koma) {
        let map = this.prediction.setKoma(koma.komaType, { x, y });
        if (koma.komaType == 'hu') {
          this.predictionMap_hu[x] = map;
        }
      }
    });

    this.predictionMap_ou = this.prediction.predictionMaps.filter(map => map.komaType == 'ou')[0];
    /*
    this.predictionMap_ou.clearBoard();
    if (this.game.playerNumber == 1) {
      this.predictionMap_ou.setPostion({ x: 4, y: 0 });
    } else {
      this.predictionMap_ou.setPostion({ x: 4, y: 8 });
    }
    */
  }

  dispose() {
    this.game = undefined;
    this.foulHands = undefined;
    this.tryPositions = undefined;
    this.moveInfoCache = undefined;
    this.prevMoveInfo = undefined;
    this.moveConvertor = undefined;
    this.invatedPosition = undefined;
    this.predictionMap_ou.dispose();
    this.predictionMap_ou = undefined;
    this.suppressionMap = undefined;
    this.invatedMap = undefined;
  }

  foul() {
    this.foulHands[JSON.stringify(this.moveInfoCache)] = true;
    this.foulCount++;
    if (this.moveInfoCache.from.komaType == 'ou') {
      this.invatedMap.surpress(this.moveInfoCache.to.position);
    }
  }

  next() {
    this.foulHands = {};
    this.tryPositions = [];
    this.useKakuCountDown -= 1;
    if (this.game.isPlayerTurn()) {
      this.prediction.moveProbability();
      
      //this.predictionMap_ou.moveProbability(this.game.turn < 15 ? 0.5 : 0.08);
      //this.predictionMap_ou.normalize();
    } else {
      this.prevGetKoma = this.state.getKoma;
      this.prevMoveInfo = this.moveInfoCache;
      

      this.toMoveInfo(this.game.board.getPlayerKoma(this.game.playerNumber), true).forEach(moveInfo => {
        this.suppressionMap.surpress(moveInfo.to.position);
        this.suppressionMap.surpress(moveInfo.from.position);
      });
    }
  }

  oute() {
    if (!this.game.isPlayerTurn()) {
      this.getOute = true;
      console.log('oute');
      let movablePositions = this.moveConvertor.getMovablePos(this.game.playerNumber, this.moveInfoCache.to.komaType, this.moveInfoCache.to.position, this.game.board);
      this.predictionMap_ou.setPositions(movablePositions);
    }
  }

  getKoma(position) {
    this.invatedPosition = position;
  }

  isOpening(): boolean {
    if (this.game.turn < 8) return true;
    if (this.game.turn > 26) return false;
    let isOpening = 45;
    let isntOpening = this.game.turn * 4;
    if (this.isKomaHitted) isntOpening += 5;
    isntOpening += this.komaHittedCount * 2;
    return Math.random() < (isOpening / (isOpening + isntOpening));
  }

  execute(): MoveInfo {
    let _komaArray: any[] = this.game.board.getPlayerKoma(this.game.playerNumber);
    let komaArray: any[] = this.game.board.getPlayerKoma(this.game.playerNumber);
    let _komadaiArray: any[] = this.game.board.getPlayerKomadai(this.game.playerNumber);
    let komadaiArray: any[] = this.game.board.getPlayerKomadai(this.game.playerNumber);
    let moveInfoArray: MoveInfo[] = null;
    let executed = false;
    let result: MoveInfo = null;
    let count = 0;
    while (!executed) {
      count++;
      let senpou = '';
      let confirmed = false;
      if (count > 15) {
        senpou = 'なにをしたら良いかわからない';
        moveInfoArray = this.toMoveInfo(_komaArray);
        confirmed = true;
      } else if (this.state.oute) {
        //王手時の対応
        senpou = '王手時の対応';

        //とりあえず駒は打たない
        komadaiArray = [];

        if (this.state.getKoma) {
          if (Math.random() > 0.2) {
            //取り返す
            moveInfoArray = this.toMoveInfo(_komaArray);
            moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
            confirmed = this.existsNotFoulHand(moveInfoArray);
          }
          if (!confirmed) {
            //王を動かす
            komaArray = this.use('ou', _komaArray);
            moveInfoArray = this.toMoveInfo(komaArray);
            confirmed = this.existsNotFoulHand(moveInfoArray);
          }
        }
        if (!confirmed) {
          //反則回数が多い状態では王を動かすことを優先する
          if (Math.random() > 0.6 - 0.06 * this.foulCount) {
            //王を動かす
            komaArray = this.use('ou', _komaArray);
            moveInfoArray = this.toMoveInfo(komaArray);

            //一度でも相手の利きのあった場所には移動しない
            let newMoveInfoArray = this.dontMoveToInvatedArea(moveInfoArray);
            if (newMoveInfoArray.length > 0) {
              moveInfoArray = newMoveInfoArray;
            }
            confirmed = this.existsNotFoulHand(moveInfoArray);
          }
          if (!confirmed) {
            //王以外を動かす
            let ou = this.game.board.getOu(this.game.playerNumber);

            //王の周りに駒を動かす
            let arroundPostions = Base.util.getArroundPos(ou.position);
            komaArray = this.dontUse('ou', _komaArray);
            moveInfoArray = this.toMoveInfo(komaArray);
            moveInfoArray = this.moveToPositions(arroundPostions, moveInfoArray);

            //一度試した場所は除外する（空き王手がらみがあるので本当はこの処理はだめ）
            let newMoveInfoArray = this.dontMoveToPositions(this.tryPositions, moveInfoArray);
            if (newMoveInfoArray.length > 0) {
              moveInfoArray = newMoveInfoArray;
            }
            confirmed = true;
          }
        }
      }

      if (!confirmed && this.state.getKoma) {
        this.recapture = false;

        //駒を取られた場合の対応
        senpou = '駒を取られた場合の対応';

        //とりあえず駒は打たない
        komadaiArray = [];

        //取り返す
        moveInfoArray = this.toMoveInfo(_komaArray);
        moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);

        if (moveInfoArray.length >= 2 && Math.random() > 0.01) {
          //２種類以上の取り方が存在している場合:ほぼ確実に取る
          confirmed = true;
          komadaiArray = [];
        } else if (moveInfoArray.length == 1 && (Math.random() > 0.2 || moveInfoArray[0].from.komaType == 'hu')) {
          //取り方が1種類:だいたいとる
          confirmed = true;
          komadaiArray = [];
        }

        if (!confirmed && Math.random() > 0.5) {
          //安い駒で取り返せるように駒を動かす
          console.log('安い駒で取り返せるように駒を動かす');
          komaArray = this.dontUse('ou', _komaArray.concat(_komadaiArray));
          komaArray = this.dontUse('hi', komaArray);
          komaArray = this.dontUse('ka', komaArray);
          komaArray = this.dontUse('ki', komaArray);
          komaArray = this.dontUse('gi', komaArray);
          moveInfoArray = this.toMoveInfo(komaArray);
          moveInfoArray = this.targetToPosition(this.invatedPosition, moveInfoArray);
          if (moveInfoArray.length > 0) {
            confirmed = true;
            komadaiArray = [];
            this.recapture = true;
            this.recapturePosition = this.invatedPosition;
          }
        }
      }

      if (!confirmed && this.recapture) {
        //取り返す
        confirmed = true;
        this.recapture = false;
        moveInfoArray = this.toMoveInfo(_komaArray);
        moveInfoArray = this.moveToPosition(this.recapturePosition, moveInfoArray);
      }

      if (!confirmed && this.prevGetKoma && !this.state.getKoma && Math.random() > 0.2) {
        //駒を取った直後の対応
        senpou = '駒を取った直後の対応';

        if (Math.random() > 0.5) {
          //指定した場所から動かす(取り返されないように駒をかわす)
          moveInfoArray = this.toMoveInfo(_komaArray);
          moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
        } else {
          //駒にひもをつける
          moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
          moveInfoArray = this.targetToPosition(this.prevMoveInfo.to.position, moveInfoArray);

          if (moveInfoArray.length == 0) {
            //指定した場所から動かす(取り返されないように駒をかわす)
            moveInfoArray = this.toMoveInfo(_komaArray);
            moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
          }
        }
        confirmed = true;
      }
      if (!confirmed && this.isOpening()) {
        //序盤の対応
        senpou = '序盤の対応';
        if (this.game.turn > 4 && Math.random() > 0.65) {
          //王を動かす
          komaArray = this.use('ou', _komaArray);

          moveInfoArray = this.toMoveInfo(komaArray);
          //5筋から離れる
          moveInfoArray = this.leaveCenterColumn(moveInfoArray);
          //後退しない
          moveInfoArray = this.maynotBack(moveInfoArray);
        } else {
          let kaku = this.game.board.get(this.game.playerNumber, 'ka');
          if (kaku != null && (!this.moveHuForKakuInOpening || !this.moveKakuInOpening) && Math.random() > 0.2) {
            //飛車先の特攻に備えて角を逃がす
            if (!this.moveHuForKakuInOpening) {
              //角道を開ける歩を動かす
              this.moveHuForKakuInOpening = true;
              komaArray = this.use('hu', _komaArray);
              komaArray = komaArray.filter(koma => koma.position.x == kaku.position.x + 1 || koma.position.x == kaku.position.x - 1);
            } else {
              //角を動かす
              this.moveKakuInOpening = true;
              komaArray = this.use('ka', _komaArray);
            }

            //候補手の展開
            moveInfoArray = this.toMoveInfo(komaArray);
            //角を動かすのは3マスまで
            moveInfoArray = this.moveShort(moveInfoArray, 3);
          } else {
            //王を使わない
            komaArray = this.dontUse('ou', _komaArray);
            //香車を使わない
            komaArray = this.dontUse('ky', komaArray);
            //序盤は一度しか飛車を動かさない
            if (this.moveHisyaInOpening) {
              console.log(' dontUse hi');
              komaArray = this.dontUse('hi', komaArray);
            }

            //序盤は一度しか角を動かさない
            if (this.moveKakuInOpening) {
              console.log(' dontUse ka');
              komaArray = this.dontUse('ka', komaArray);
            }

            //候補手の展開
            moveInfoArray = this.toMoveInfo(komaArray);

            //金銀は繰り出す
            moveInfoArray = this.advanceKinGin2(moveInfoArray);
            //飛車は端に移動しない
            moveInfoArray = this.dontMoveHisyaToEnd(moveInfoArray);
          }
        }
        confirmed = true;
      }

      if (!confirmed) {
        //その他の場合
        senpou = 'その他の場合';

        if (Math.random() > 0.5) {
          //王を使わない
          komaArray = this.dontUse('ou', _komaArray);
        } else {
          //角を闇雲に動かさない
          komaArray = this.dontUse('ka', _komaArray);
          //komaArray = _komaArray;
        }

        //反則回数が多いときは持ち駒の使用を控える
        if (this.foulCount > 7) {
          moveInfoArray = this.toMoveInfo(komaArray);
        } else {
          moveInfoArray = this.toMoveInfo(komaArray.concat(_komadaiArray));
        }

        if (this.game.turn < 35 && Math.random() > 0.5) {
          console.log('混合戦略0：飛車先の歩を突く or 最前線の歩を突く');
          //飛車先の歩を突く
          let moveInfoArray0 = [];
          let hisya = this.game.board.get(this.game.playerNumber, 'hi');
          if (hisya != null) {
            moveInfoArray0 = this.proceedHu(moveInfoArray, hisya.position.x);
          }
          
          komaArray = this.dontUseEnd(_komaArray);
          komaArray = this.useTop('hu', komaArray);
          moveInfoArray = moveInfoArray0.concat(this.toMoveInfo(komaArray));
        } else if (Math.random() > 0.4) {
          console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');

          //制圧済み領域を広げる(まだ王手をかけたことがなければ)
          let moveInfoArray1 = [];
          if(!this.getOute) {
            moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);
          }
          
          if (Math.random() > 0.4) {
            //金銀を繰り出す
            let moveInfoArray2 = this.advanceKinGin(moveInfoArray);
            moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
          } else {
            moveInfoArray = moveInfoArray1;
          }

        } else {
          console.log('混合戦略2：と金をつくる or 王の探索効率が良い手を指す');
          //と金をつくる手
          let moveInfoArray_tokin = this.makeTokin(moveInfoArray);
          //探索範囲の最大の手
          let moveInfoArray_most = this.mostEffective(moveInfoArray, 5);

          moveInfoArray = moveInfoArray_tokin.concat(moveInfoArray_most);

          if (moveInfoArray.length == 0) {
            console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
            //制圧済み領域を広げる
            let moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);

            if (Math.random() > 0.4) {
              //金銀を繰り出す
              let moveInfoArray2 = this.advanceKinGin(moveInfoArray);
              moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
            } else {
              moveInfoArray = moveInfoArray1;
            }
          }
        }

        //反則回数が多いときは駒の長距離移動は避ける
        if (this.foulCount > 6) {
          moveInfoArray = this.moveShort(moveInfoArray, 2);
        }

        if (moveInfoArray.length == 0) {
          console.log('-> 予備：金銀は繰り出す');
          moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
          moveInfoArray = this.advanceKinGin2(moveInfoArray);
        }
      }

      moveInfoArray = this.dontRedoFoulHand(moveInfoArray);
      getRandom(moveInfoArray, moveInfo => {
        this.moveInfoCache = moveInfo;
        result = moveInfo;
        executed = true;
        console.log(this.game.turn + ' : ' + senpou);
        console.log(JSON.stringify(moveInfo));
        console.log('他候補 : ' + moveInfoArray.length);
        if (senpou == '序盤の対応' && moveInfo.from.komaType == 'hi') {
          this.moveHisyaInOpening = true;
        }
        if (senpou == '王手時の対応' && moveInfo.from.komaType != 'ou') {
          this.tryPositions.push(moveInfo.to.position);
        }
        if(moveInfo.to.komaType == 'ka') {
          this.useKakuCountDown = 8;
        }
      });
    }

    return result;
  }

  toMoveInfo(komaArray: any[], short: boolean = false): MoveInfo[] {
    let result: MoveInfo[] = [];
    komaArray.forEach(koma => {
      let isKomadai = this.isKomadai(koma.position);
      let movablePosArray = isKomadai
        ? this.moveConvertor.getPutableCell(this.game.playerNumber, koma.koma.komaType, this.game.board)
        : short
          ? this.moveConvertor.getMovablePosShort(this.game.playerNumber, koma.koma.komaType, koma.position, this.game.board)
          : this.moveConvertor.getMovablePos(this.game.playerNumber, koma.koma.komaType, koma.position, this.game.board);
      movablePosArray.forEach(movablePos => {
        let moveInfo: MoveInfo = {
          from: {
            position: koma.position,
            komaType: koma.koma.komaType
          },
          to: {
            position: movablePos,
            komaType: koma.koma.komaType
          }
        };
        if (!isKomadai) {
          let nari = this.moveConvertor.getNari(koma.koma.playerNumber, koma.koma.komaType, moveInfo.from.position, moveInfo.to.position);
          if (nari == 'force' || nari == 'possible') {
            moveInfo.to.komaType = Base.komaTypes[moveInfo.to.komaType].nariId;
          }
        }
        result.push(moveInfo);
      });
    });
    return result;
  }

  toMovablePositions(moveInfo: MoveInfo) {
    return this.moveConvertor.getMovablePos(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
  }

  //座標が(-1, -1)であるかどうかを判定する
  isKomadai(position: { x: number, y: number }) {
    return position.x == -1 && position.y == -1;
  }

  //前に進める動きかどうかを判定する
  isAdvance(moveInfo: MoveInfo) {
    if (this.game.playerNumber == 1) {
      return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0;
    } else {
      return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0;
    }
  }

  //特定の駒を使わない
  dontUse(komaType: string, komaArray: any[]) {
    return komaArray.filter(koma => koma.koma.komaType != komaType);
  }

  //特定の駒を使う
  use(komaType: string, komaArray: any[]) {
    return komaArray.filter(koma => koma.koma.komaType == komaType);
  }

  //最前線の駒を使う
  useTop(komaType: string, komaArray: any[]) {
    let newKomaArray = komaArray.filter(koma => koma.koma.komaType == komaType);
    if (newKomaArray.length <= 1) {
      return newKomaArray;
    } else {
      if (this.game.playerNumber == 1) {
        return [newKomaArray.sort((k1, k2) => k1.position.y - k2.position.y)[0]];
      } else {
        return [newKomaArray.sort((k1, k2) => k2.position.y - k1.position.y)[0]];
      }

    }
  }
  
  //角を闇雲に動かさない
  dontUseKakuMany(komaArray: any[]){
    if(this.useKakuCountDown <= 0) {
      return komaArray;
    } else {
      return this.dontUse('kaku', komaArray);
    }
  }
  
  //端の駒を使わない
  dontUseEnd(komaArray: any[]) {
    return komaArray.filter(koma => koma.position.x != 0 &&koma.position.x != 8);
  }

  //後退しない
  dontBack(moveInfoArray: MoveInfo[]) {
    if (this.game.playerNumber == 1) {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0);
    } else {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0);
    }
  }

  //あまり後退しない
  maynotBack(moveInfoArray: MoveInfo[]) {
    if (Math.random() > 0.5) return moveInfoArray;
    if (this.game.playerNumber == 1) {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y <= 0);
    } else {
      return moveInfoArray.filter(moveInfo => this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y >= 0);
    }
  }

  //5筋から離れる
  leaveCenterColumn(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      if (moveInfo.from.position.x == 4) {
        return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x != 0;
      } else if (moveInfo.from.position.x > 4) {
        return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x >= 0;
      } else {
        return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x <= 0;
      }
    });
  }

  //指定した場所から動かす
  moveFromPosition(postion: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => Base.util.posEq(moveInfo.from.position, postion));
  }

  //指定した場所から動かす
  moveFromPositions(positions: { x: number, y: number }[], moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => positions.some(position => Base.util.posEq(moveInfo.from.position, position)));
  }

  //指定した場所に動かす
  moveToPosition(postion: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => Base.util.posEq(moveInfo.to.position, postion));
  }

  //指定した場所に動かす2
  moveToPositions(positions: { x: number, y: number }[], moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => positions.some(position => Base.util.posEq(moveInfo.to.position, position)));
  }

  //指定した場所に動かさない
  dontMoveToPosition(postion: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => !Base.util.posEq(moveInfo.to.position, postion));
  }

  //指定した場所に動かさない2
  dontMoveToPositions(positions: { x: number, y: number }[], moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => !positions.some(position => Base.util.posEq(moveInfo.to.position, position)));
  }

  //一度反則になった手を指さない
  dontRedoFoulHand(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      return !this.foulHands.hasOwnProperty(JSON.stringify(moveInfo));
    });
  }

  //反則になった手以外の手があるかどうか
  existsNotFoulHand(moveInfoArray: MoveInfo[]) {
    return !moveInfoArray.every(moveInfo => {
      return this.foulHands.hasOwnProperty(JSON.stringify(moveInfo));
    });
  }

  //移動距離を制限する
  moveShort(moveInfoArray: MoveInfo[], length: number) {
    return moveInfoArray.filter(moveInfo => {
      return Math.abs(moveInfo.from.position.x - moveInfo.to.position.x) <= length
        && Math.abs(moveInfo.from.position.y - moveInfo.to.position.y) <= length;
    });
  }

  //一度でも相手の利きのあった場所には移動しない
  dontMoveToInvatedArea(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      return !this.invatedMap.hasSuppressed(moveInfo.to.position);
    });
  }

  //指定した場所にひもがつくようにする
  targetToPosition(position: { x: number, y: number }, moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      let movablepositions = this.toMovablePositions(moveInfo);
      return movablepositions.some(p => Base.util.posEq(p, position));
    });
  }

  //制圧済み領域が広がる手を指す
  expandSuppressedArea(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      return this.toMovablePositions(moveInfo).some(p => !this.suppressionMap.hasSuppressed(p));
    });
  }

  //王の探索範囲が変わらない手を指さない
  dontMoveUseless(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      let movablePositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
      let sum = 0;
      movablePositions.forEach(p => {
        sum += this.predictionMap_ou.board[p.y][p.x];
      });
      return sum != 0;
    });
  }

  //王の探索効率が最も良い手を指す
  mostEffective(moveInfoArray: MoveInfo[], rank: number) {
    let top = 0;
    let topMoveInfo = null;
    let sorted = moveInfoArray.sort((m1, m2) => {
      let movablePositions1 = this.moveConvertor.getMovablePosShort(this.game.playerNumber, m1.to.komaType, m1.to.position, this.game.board);
      let sum1 = 0;
      movablePositions1.forEach(p => {
        sum1 += this.predictionMap_ou.board[p.y][p.x];
      });

      let movablePositions2 = this.moveConvertor.getMovablePosShort(this.game.playerNumber, m2.to.komaType, m2.to.position, this.game.board);
      let sum2 = 0;
      movablePositions1.forEach(p => {
        sum2 += this.predictionMap_ou.board[p.y][p.x];
      });
      return sum1 - sum2;
    });
    /*
    moveInfoArray.forEach(moveInfo => {
      let movablePositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
      let sum = 0;
      movablePositions.forEach(p => {
        sum += this.predictionMap_ou.board[p.y][p.x];
      });
      if (sum > top) {
        top = sum;
        topMoveInfo = moveInfo;
      }
    });
    */
    /*
    if (topMoveInfo == null) {
      return [];
    } else {
      return [topMoveInfo];
    }
    */
    if (sorted.length < rank) {
      return sorted;
    } else {
      return sorted.slice(0, rank);
    }
  }

  //飛車は端に移動しない
  dontMoveHisyaToEnd(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => moveInfo.from.komaType != 'hi' || (moveInfo.to.position.x != 0 && moveInfo.to.position.x != 8));
  }

  //と金つくりの手を抽出する
  makeTokin(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => moveInfo.from.komaType == 'hu' && moveInfo.from.komaType == 'to');
  }

  //金銀を繰り出す
  advanceKinGin(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
        return this.isAdvance(moveInfo);
      }
      return false;
    });
  }

  //金銀は繰り出す
  advanceKinGin2(moveInfoArray: MoveInfo[]) {
    return moveInfoArray.filter(moveInfo => {
      if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
        return this.isAdvance(moveInfo);
      }
      return true;
    });
  }

  //特定の筋の歩を突く
  proceedHu(moveInfoArray: MoveInfo[], x: number) {
    return moveInfoArray.filter(moveInfo => moveInfo.from.komaType == 'hu' && moveInfo.from.position.x == x);
  }
}

export class Prediction {
  predictionMaps: PredictionMap[] = [];
  moveConvertor: Base.MoveConvertor;
  game: Base.Game;
  state: GameState;
  strategy: Strategy;

  getHuColumns: boolean[] = [false, false, false, false, false, false, false, false, false];
  invatedHuColumns: boolean[] = [false, false, false, false, false, false, false, false, false];
  constructor(moveConvertor: Base.MoveConvertor, game: Base.Game, state: GameState, strategy: Strategy) {
    this.moveConvertor = moveConvertor;
    this.game = game;
    this.state = state;
    this.strategy = strategy;
  }

  setKoma(komaType: string, position: { x: number, y: number }): PredictionMap {
    let map = new PredictionMap(this, komaType, 3 - this.game.playerNumber, this.moveConvertor, this.game, this.state, this.strategy);
    map.setPosition(position);
    this.predictionMaps.push(map);
    return map;
  }

  setKomadai(komaType: string) {
    let map = new PredictionMap(this, komaType, 3 - this.game.playerNumber, this.moveConvertor, this.game, this.state, this.strategy);
    map.setKomadai();
    this.predictionMaps.push(map);
  }

  moveProbability() {

    let existRateBoard = this.getExistRateMap();
    if (!this.state.getKoma) {
      let rate = 1 / this.predictionMaps.length;
      this.predictionMaps.forEach(map => {
        map.moveProbability(rate, existRateBoard);
      });
      this.predictionMaps.forEach(map => {
        map.normalize();
      });
    } else {
      let rateSum = this.predictionMaps.reduce((sum, map) => sum + map.moveProbabilityGetRate(existRateBoard), 0);
      this.predictionMaps.forEach(map => {
        map.moveProbabilityGet(existRateBoard, rateSum);
      });
      this.predictionMaps.forEach(map => {
        map.normalizeWith(rateSum);
      });
    }

  }

  converge(komaType: string, position: { x: number, y: number }) {
    console.log('converge : ' + komaType);

    let resourceId = Base.komaTypes[komaType].resourceId;
    let komaps = this.predictionMaps.filter(komap =>
      Base.komaTypes[komap.komaType].resourceId == resourceId
      && komap.board[position.y][position.x] > 0);
    console.log('komaps.length : ' + komaps.length);
    if (komaps.length == 1) {
      this.predictionMaps = this.predictionMaps.filter(komap =>
        Base.komaTypes[komap.komaType].resourceId != resourceId
        || komap.board[position.y][position.x] == 0);
    } else {
      let rateSum = komaps.reduce((prev, komap) => prev + komap.board[position.y][position.x], 0);
      komaps.forEach(komap => {
        komap.getMaybe(komap.board[position.y][position.x] / rateSum);
      });
    }

    if (komaType == 'hu') {
      this.getHuColumns[position.x] = true;
    }
  }

  getExistRateMap() {
    let board = getNewBoard();
    Base.util.allPos((x, y) => {
      let notExistRate = this.predictionMaps.reduce((sum, komap) => sum * (1 - komap.board[y][x]), 1);
      board[y][x] = 1 - notExistRate;
    });
    return board;
  }
}

export class PredictionMap {
  prediction: Prediction;
  komaType: string;
  komaId: string;
  playerNumber: number;
  board: number[][];
  nextBoard: number[][];
  yRange: { min: number, max: number };
  komadaiRate: number = 0;
  moveConvertor: Base.MoveConvertor;
  game: Base.Game;
  state: GameState;
  strategy: Strategy;
  decisionPoint: { x: number, y: number }
  gettedRate: number = 0;
  rateSum: number; //getKoma時のメモ用
  constructor(prediction: Prediction, komaType: string, playerNumber: number, moveConvertor: Base.MoveConvertor, game: Base.Game, state: GameState, strategy: Strategy) {
    this.prediction = prediction;
    this.komaType = komaType;
    this.playerNumber = playerNumber;
    this.komaId = Base.komaTypes[komaType].resourceId;
    this.moveConvertor = moveConvertor;
    this.game = game;
    this.state = state;
    this.strategy = strategy;
  }

  dispose() {
    this.board = undefined;
    this.moveConvertor = undefined;
    this.game = undefined;
    this.state = undefined;
    this.strategy = undefined;
  }

  clearBoard() {
    this.board = getNewBoard();
  }

  getMaybe(rate: number) {
    this.gettedRate = 1 - (1 - this.gettedRate) * rate;
  }

  setKomadai() {
    this.board = getNewBoard();;
    this.komadaiRate = 1;
  }

  setPosition(position: { x: number, y: number }) {
    this.clearBoard();
    this.board[position.y][position.x] = 1;
    this.decisionPoint = position;
    this.yRange = { min: position.y, max: position.y };
  }

  setPositions(positions: { x: number, y: number }[]) {
    let board = getNewBoard();
    positions.forEach(p => {
      board[p.y][p.x] = this.board[p.y][p.x] / positions.length;
    });
    this.nextBoard = board;
    this.normalize();
  }

  normalize() {
    let sum = 0;
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.nextBoard[y][x] < 0.0000000001) this.nextBoard[y][x] = 0;
        sum += this.nextBoard[y][x];
      }
    }
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.nextBoard[y][x] == 0) continue;
        this.nextBoard[y][x] = this.nextBoard[y][x] / sum * (1 - this.gettedRate);
        if (this.nextBoard[y][x] != 0) {
          if (this.yRange.min > y) this.yRange.min = y;
          if (this.yRange.max < y) this.yRange.max = y;
        }
      }
    }
    this.board = this.nextBoard;
  }

  moveProbabilityGetRate(existRateBoard) {
    let sum = 0;
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        let movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
        movablePositions.forEach(pos => {
          //香車などによる追い越し禁止
          if (this.isPassed(x, y, pos)) return;
          if (Base.util.posEq(pos, this.strategy.invatedPosition)) {
            sum += this.board[y][x];
          }
        });
      }
    }
    this.rateSum = sum;
    return sum;
  }

  moveProbabilityGet(existRateBoard, rateSum) {
    if (this.rateSum == 0) {
      return;
    }
    let board = getNewBoard();
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        let movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
        movablePositions.forEach(pos => {
          //香車などによる追い越し禁止
          if (this.isPassed(x, y, pos)) return;
          if (Base.util.posEq(pos, this.strategy.invatedPosition)) {
            board[this.strategy.invatedPosition.y][this.strategy.invatedPosition.x] += this.board[y][x] * rateSum / this.rateSum;
          }
        });
        board[y][x] += this.board[y][x] * (1 - this.rateSum / rateSum);
      }
    }
    this.nextBoard = board;
  }

  normalizeWith(rateSum: number) {
    if (this.rateSum == 0) {
      return;
    }
    let sum = 0;
    let position = this.strategy.invatedPosition;
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (x == position.x && y == position.y) continue;
        sum += this.nextBoard[y][x];
      }
    }
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.nextBoard[y][x] == 0) continue;
        this.nextBoard[y][x] = this.nextBoard[y][x] / sum * (1 - this.gettedRate) * ((1 - this.rateSum / rateSum));
      }
    }
    this.nextBoard[position.y][position.x] = this.rateSum / rateSum;
    this.board = this.nextBoard;
  }

  moveProbability(rate, existRateBoard) {
    let board = getNewBoard();
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.board[y][x] == 0) continue;
        let movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
        let rateSum = movablePositions.reduce((sum, pos) => sum + (1 - existRateBoard[pos.y][pos.x]), 0);
        if (rateSum != 0) {
          movablePositions.forEach(pos => {
            //香車などによる追い越し禁止
            if (this.isPassed(x, y, pos)) return;
            board[pos.y][pos.x] += rate / (rateSum / (1 - existRateBoard[pos.y][pos.x])) * this.board[y][x];
          });
        }
        board[y][x] += this.board[y][x] * (1 - rate);
      }
    }

    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.game.board.getKoma({ x: x, y: y })) {
          board[y][x] = 0;
        }
      }
    }
    this.nextBoard = board;
    let komaArray: any[] = this.game.board.getPlayerKoma(this.game.playerNumber);
    if (this.komaType == 'ou') {
      komaArray.forEach(koma => {
        let movablepositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, koma.koma.komaType, koma.position, this.game.board);
        movablepositions.forEach(p => {
          this.nextBoard[p.y][p.x] = 0;
        });
      });
    }
  }

  isPassed(fromX, fromY, pos) {
    if (!this.decisionPoint) return false;
    if (Math.abs(pos.y - fromY) >= 2) {
      if (!this.prediction.getHuColumns[fromX]) {
        let hu = this.strategy.predictionMap_hu[fromX];
        if (this.isBackward(this.decisionPoint.y, hu.decisionPoint.y)) {
          if (this.isBackward(hu.getMaxY(), pos.y)) {
            return true;
          } else {
          }
        }
      }
    }
    return false;
  }

  isBackward(targetY, comparisonY) {
    if (this.playerNumber == 1) {
      return targetY >= comparisonY;
    } else {
      return targetY <= comparisonY;
    }
  }

  getMaxY() {
    if (this.playerNumber == 1) {
      return this.yRange.min;
    } else {
      return this.yRange.max;
    }
  }
}



class SuppressionMap {
  board: number[][]
  booleanBoard: boolean[][]
  constructor() {
    this.board = this.getNewBoard();
    this.booleanBoard = this.getNewBoolenaBoard();
  }
  getNewBoard() {
    return [
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
  }

  getNewBoolenaBoard() {
    return [
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false],
      [false, false, false, false, false, false, false, false, false]
    ];
  }

  surpress(position: { x: number; y: number }) {
    this.board[position.y][position.x] = -1;
    this.booleanBoard[position.y][position.x] = true;
  }

  next() {
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        this.board[y][x]++;
      }
    }
  }

  getUnsuppressedPosition() {
    let result: { x: number, y: number }[] = [];
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (!this.booleanBoard[y][x]) {
          result.push({ x: x, y: y });
        }
      }
    }
    return result;
  }

  hasSuppressed(position: { x: number, y: number }): boolean {
    return this.booleanBoard[position.y][position.x];
  }
}


let getNewBoard = () => {
  return [
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0]
  ];
}