
export class util {
  static isVacant(val) {
    return val == null || val == undefined || val == "" || val == 0;
  }
  static isntVacant(val) {
    return val != null && val != undefined && val != "" && val != 0;
  }
  static reverseVector(position, playerNumber) {
    if (playerNumber == 1) return position;
    return { x: position.x * -1, y: position.y * -1, length: position.length };
  }
  static posEq(p1, p2) {
    return p1.x == p2.x && p1.y == p2.y;
  }
  static posAdd(p, v) {
    return { x: p.x + v.x, y: p.y + v.y, length: v.length }
  }
  static posSub(p, v) {
    return { x: p.x - v.x, y: p.y - v.y, length: v.length }
  }
  static posMul(p, times) {
    return { x: p.x * times, y: p.y * times, length: times }
  }
  static posDup(p) {
    return { x: p.x, y: p.y, length: p.length }
  }
  static posAdd1(p) {
    return { x: p.x + 1, y: p.y + 1, length: p.length };
  }
  static posFlip(p, flip) {
    return flip ? { x: 8 - p.x, y: 8 - p.y } : { x: p.x, y: p.y };
  }
  static posFlipX(p) {
    return { x: 8 - p.x, y: p.y };
  }
  static posToStr(p) {
    return p.x + ":" + p.y;
  }
  static isInRange(pos) {
    return 0 <= pos.x && pos.x < 9 && 0 <= pos.y && pos.y < 9;
  }
  static allPos(predicate) {
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        predicate(x, y)
      }
    }
  }
  static getArroundPos(p) {
    return ([
      { x: p.x - 1, y: p.y - 1 },
      { x: p.x + 0, y: p.y - 1 },
      { x: p.x + 1, y: p.y - 1 },
      { x: p.x - 1, y: p.y + 0 },
      { x: p.x + 1, y: p.y + 0 },
      { x: p.x - 1, y: p.y + 1 },
      { x: p.x + 0, y: p.y + 1 },
      { x: p.x + 1, y: p.y + 1 }
    ]).filter(pos => util.isInRange(pos));
  }

  static find(array, predicate) {
    if (array == null) {
      return null;
    }
    if (typeof predicate !== 'function') {
      return null;
    }
    var list = Object(array);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return null;
  }
}

export class KomaType {
  id: string;
  name: string;
  nari: boolean;
  resourceId: string;
  nariId: string;
  moveDef :any;
  value: string;
  constructor(id, name, nari, resourceId, nariId, moveDef, value) {
    this.id = id;
    this.name = name;
    this.nari = nari;
    this.resourceId = resourceId;
    this.nariId = nariId;
    this.moveDef = moveDef;
    this.value = value;
  }
}

export let komaTypes : { [komaType: string] : KomaType } = (() => {
  let _komaTypes : { [komaType: string] : KomaType } = {};
  let setResource = (id, name, nari, resourceId, nariId, moveDef, value) => {
    _komaTypes[id] = new KomaType(id, name, nari, resourceId, nariId, moveDef, value)
  };
  setResource("ou", "玉", false, "ou", "", { str: "12346789", moves: ["1", "2", "3", "4", "6", "7", "8", "9"] }, 9)
  setResource("ki", "金", false, "ki", "", { str: "123468", moves: ["1", "2", "3", "4", "6", "8"] }, 5)
  setResource("gi", "銀", true, "gi", "ng", { str: "12379", moves: ["1", "2", "3", "7", "9"] }, 4)
  setResource("ng", "成銀", false, "gi", "", { str: "123468", moves: ["1", "2", "3", "4", "6", "8"] }, 4)
  setResource("ke", "桂", true, "ke", "nk", { str: "ab", moves: ["a", "b"] }, 3)
  setResource("nk", "成桂", false, "ke", "", { str: "123468", moves: ["1", "2", "3", "4", "6", "8"] }, 3)
  setResource("ky", "香", true, "ky", "ny", { str: "2_", moves: ["2_"] }, 2)
  setResource("ny", "成香", false, "ky", "", { str: "123468", moves: ["1", "2", "3", "4", "6", "8"] }, 2)
  setResource("hi", "飛", true, "hi", "ry", { str: "2_4_6_8_", moves: ["2_", "4_", "6_", "8_"] }, 7)
  setResource("ry", "竜", false, "hi", "", { str: "12_34_6_78_9", moves: ["1", "2_", "3", "4_", "6_", "7", "8_", "9"] }, 7)
  setResource("ka", "角", true, "ka", "um", { str: "1_3_7_9_", moves: ["1_", "3_", "7_", "9_"] }, 6)
  setResource("um", "馬", false, "ka", "", { str: "1_23_467_89_", moves: ["1_", "2", "3_", "4", "6", "7_", "8", "9_"] }, 6)
  setResource("hu", "歩", true, "hu", "to", { str: "2", moves: ["2"] }, 1)
  setResource("to", "と", false, "hu", "", { str: "123468", moves: ["1", "2", "3", "4", "6", "8"] }, 1)
  return _komaTypes;
})();

let getKomaName = (id) => {
  if (komaTypes[id].resourceId)
    komaTypes[komaTypes[id].resourceId].name
  else
    komaTypes[id].name
};

export class MoveConvertor {
  moveDef: any[][];
  moveDef1: any;
  moveDef2: any;
  moveKeysShort: any;
  moveKeysLong: any;
  constructor() {
    let create8Aray = (v) => {
      return ([1, 2, 3, 4, 5, 6, 7, 8]).map((i) => util.posMul(v, i))
    }
    this.moveDef = [
      ["1", [{ x: 1, y: -1, length: 1 }]],
      ["2", [{ x: 0, y: -1, length: 1 }]],
      ["3", [{ x: -1, y: -1, length: 1 }]],
      ["4", [{ x: 1, y: 0, length: 1 }]],
      ["6", [{ x: -1, y: 0, length: 1 }]],
      ["7", [{ x: 1, y: 1, length: 1 }]],
      ["8", [{ x: 0, y: 1, length: 1 }]],
      ["9", [{ x: -1, y: 1, length: 1 }]],
      ["a", [{ x: 1, y: -2, length: 1 }]],
      ["b", [{ x: -1, y: -2, length: 1 }]],
      ["1_", create8Aray({ x: 1, y: -1 })],
      ["2_", create8Aray({ x: 0, y: -1 })],
      ["3_", create8Aray({ x: -1, y: -1 })],
      ["4_", create8Aray({ x: 1, y: 0 })],
      ["6_", create8Aray({ x: -1, y: 0 })],
      ["7_", create8Aray({ x: 1, y: 1 })],
      ["8_", create8Aray({ x: 0, y: 1 })],
      ["9_", create8Aray({ x: -1, y: 1 })]
    ];
    this.moveDef1 = {};
    this.moveDef2 = {};
    this.moveDef.forEach((pair) => {
      this.moveDef1[pair[0]] = pair[1];
      this.moveDef2[pair[0]] = pair[1].map((v) => util.reverseVector(v, 2));
    });
    this.moveKeysShort = { "1": true, "2": true, "3": true, "4": true, "6": true, "7": true, "8": true, "9": true, "a": true, "b": true }
    this.moveKeysLong = { "1_": true, "2_": true, "3_": true, "4_": true, "6_": true, "7_": true, "8_": true, "9_": true }
  }


  cutLength(playerNumber, positions: any[], gameBoard) {
    let ngLength = 0;
    let ngPos = util.find(positions, (pos) => util.isntVacant(gameBoard.getKoma(pos)));
    if (ngPos) {
      var toKoma = gameBoard.getKoma(ngPos);
      ngLength = toKoma.playerNumber == playerNumber ? ngPos.length : ngPos.length + 1;
    }
    return ngLength != 0 ? positions.filter((pos) => pos.length < ngLength) : positions;
  }

  getMovablePos(playerNumber, komaType, position, gameBoard): any[] {
    let moves: string[] = komaTypes[komaType].moveDef.moves;
    let _moveDef = playerNumber == 1 ? this.moveDef1 : this.moveDef2;
    let result = [];
    moves.forEach((move) => {
      let positions = _moveDef[move].map((v) => util.posAdd(position, v));
      positions = positions.filter((pos) => util.isInRange(pos));
      positions = this.cutLength(playerNumber, positions, gameBoard)
      result = result.concat(positions)
    });
    return result;
  }

  getMovablePosShort(playerNumber, komaType, position, gameBoard) {
    let moves: string[] = komaTypes[komaType].moveDef.moves;
    moves = moves.map(move => move.replace("_", ""));
    let _moveDef = playerNumber == 1 ? this.moveDef1 : this.moveDef2;
    let result = [];
    moves.forEach((move) => {
      let positions = _moveDef[move].map((v) => util.posAdd(position, v));
      positions = positions.filter((pos) => util.isInRange(pos));
      positions = this.cutLength(playerNumber, positions, gameBoard)
      result = result.concat(positions)
    });
    return result;
  }

  getPutableCell(playerNumber, komaType, gameBoard) {
    let positions = []
    util.allPos((x, y) => {
      if (util.isVacant(gameBoard.getKoma({ x: x, y: y }))) {
        positions.push({ x: x, y: y })
      }
    });

    let filter = null;
    if (komaType == "hu" || komaType == "ky") {
      filter = playerNumber == 1 ? (p) => p.y != 0 : (p) => p.y != 8;
    }

    if (komaType == "ke") {
      filter = playerNumber == 1 ? (p) => p.y > 1 : (p) => p.y < 7;
    }

    if (komaType == "hu") {
      let columns = gameBoard.getExistsHuColumn(playerNumber);
      positions = positions.filter(p => columns[p.x] ? false : true);
    }

    return filter ? positions.filter(filter) : positions;
  }


  getNari(playerNumber, komaType, fromPos, toPos) {
    if (komaTypes[komaType].nari == false) return "none";

    if (playerNumber == 1) {
      if (fromPos.y < 3 || toPos.y < 3) {
        if ((komaType == "hu" || komaType == "ky") && toPos.y == 0) return "force";
        if (komaType == "ke" && toPos.y < 2) return "force";
        return "possible";
      } else {
        return "none";
      }

    } else {
      if (fromPos.y > 5 || toPos.y > 5) {
        if ((komaType == "hu" || komaType == "ky") && toPos.y == 8) return "force";
        if (komaType == "ke" && toPos.y > 6) return "force";
        return "possible";
      } else {
        return "none";
      }

    }
  }

}
export let moveConvertor = new MoveConvertor();


export class Koma {
  playerNumber: number;
  komaType: string;
  constructor(playerNumber, komaType) {
    this.playerNumber = playerNumber;
    this.komaType = komaType;
  }
  static km1 = (komaType) => new Koma(1, komaType);
  static km2 = (komaType) => new Koma(2, komaType);
}


export class Board {
  player1;
  player2;
  komaDai;
  board;
  constructor(player1, player2) {
    this.player1 = player1;
    this.player2 = player2;
    this.komaDai = [[], []];
    this.board = [[], [], [], [], [], [], [], [], []];
  }


  clearBoard() {
    this.board = [[], [], [], [], [], [], [], [], []];
  }
  initBoard(playerNumber) {
    if (playerNumber == 1) {
      this.board = [
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        [Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu')],
        ['', Koma.km1('hi'), '', '', '', '', '', Koma.km1('ka'), ''],
        [Koma.km1('ky'), Koma.km1('ke'), Koma.km1('gi'), Koma.km1('ki'), Koma.km1('ou'), Koma.km1('ki'), Koma.km1('gi'), Koma.km1('ke'), Koma.km1('ky')]
      ];
    } else if (playerNumber == 2) {
      this.board = [
        [Koma.km2('ky'), Koma.km2('ke'), Koma.km2('gi'), Koma.km2('ki'), Koma.km2('ou'), Koma.km2('ki'), Koma.km2('gi'), Koma.km2('ke'), Koma.km2('ky')],
        ['', Koma.km2('ka'), '', '', '', '', '', Koma.km2('hi'), ''],
        [Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu')],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', '']
      ];
    } else {
      this.board = [
        [Koma.km2('ky'), Koma.km2('ke'), Koma.km2('gi'), Koma.km2('ki'), Koma.km2('ou'), Koma.km2('ki'), Koma.km2('gi'), Koma.km2('ke'), Koma.km2('ky')],
        ['', Koma.km2('ka'), '', '', '', '', '', Koma.km2('hi'), ''],
        [Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu'), Koma.km2('hu')],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        ['', '', '', '', '', '', '', '', ''],
        [Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu'), Koma.km1('hu')],
        ['', Koma.km1('hi'), '', '', '', '', '', Koma.km1('ka'), ''],
        [Koma.km1('ky'), Koma.km1('ke'), Koma.km1('gi'), Koma.km1('ki'), Koma.km1('ou'), Koma.km1('ki'), Koma.km1('gi'), Koma.km1('ke'), Koma.km1('ky')]
      ]
    }
  }

  duplicate() {
    let board = new Board(this.player1, this.player2);
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (this.board[x][y]) {
          board.board[x][y] = new Koma(this.board[x][y].playerNumber, this.board[x][y].komaType);
        } else {
          board.board[x][y] = '';
        }
      }
    }

    board.komaDai[0] = board.komaDai[0].concat(this.komaDai[0]);
    board.komaDai[1] = board.komaDai[1].concat(this.komaDai[1]);
    return board;
  }


  getKoma(position) {
    return this.board[position.y][position.x];
  }

  getExistsHuColumn(playerNumber) {
    let result = {};
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        var koma = this.board[y][x];
        if (koma && koma.playerNumber == playerNumber && koma.komaType == "hu") {
          result[x] = true;
        }
      }
    }
    return result;
  }


  putKoma(koma, position) {
    this.board[position.y][position.x] = koma;
  }


  removeKoma(position) {
    this.board[position.y][position.x] = undefined;
  }


  getKomaFromKomadai(playerNumber, komaType) {
    let find = false
    let index = playerNumber == 1 ? 0 : 1;
    let koma = util.find(this.komaDai[index], (koma) => koma.komaType == komaType);
    this.komaDai[index] = this.komaDai[index].filter((koma) => {
      if (find) return true;
      if (koma.komaType == komaType) return !(find = true);
      return true;
    });

    return koma
  }

  putKomaToKomadai(playerNumber, koma) {
    koma = new Koma(koma.playerNumber, koma.komaType);
    koma.playerNumber = playerNumber;
    let type = komaTypes[koma.komaType];
    koma.komaType = komaTypes[koma.komaType].resourceId;
    if (playerNumber == 1) {
      this.komaDai[0].push(koma);
      this.komaDai[0] = this.komaDai[0].sort((koma) => komaTypes[koma.komaType].value);
    } else {
      this.komaDai[1].push(koma);
      this.komaDai[1] = this.komaDai[1].sort((koma) => komaTypes[koma.komaType].value);
    }
  }

  removeKomaFromKomadai(playerNumber, komaType) {
    let find = false
    let index = playerNumber == 1 ? 0 : 1;
    let koma = util.find(this.komaDai[index], (koma) => koma.komaType == komaType);
    this.komaDai[index] = this.komaDai[index].filter((koma) => {
      if (find) return true;
      if (koma.komaType == komaType) return !(find = true);
      return true;
    });
    return koma;
  }


  getKomaWithPosition(predicate) {
    let result = [];
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        var koma = this.board[y][x];
        if (koma && predicate(koma)) {
          result.push({ koma: koma, position: { x: x, y: y } });
        }
      }
    }
    return result;
  }

  getPlayerKoma(playerNumber) {
    return this.getKomaWithPosition((koma) => koma.playerNumber == playerNumber);
  }

  getPlayerKomadai(playerNumber) {
    let komadaiKoma = this.komaDai[playerNumber - 1].map(koma => {
      return { koma: koma, position: { x: -1, y: -1 } };
    });
    return komadaiKoma;
  }


  getOu(playerNumber) {
    let ou = this.getKomaWithPosition((koma) => koma.playerNumber == playerNumber && koma.komaType == "ou");
    return ou[0] ? ou[0] : null;
  }
  
  get(playerNumber, komaType) {
    let koma = this.getKomaWithPosition((koma) => koma.playerNumber == playerNumber && koma.komaType == komaType);
    return koma[0] ? koma[0] : null;
  }


  static create(rowBord, playerInfo) {
    let board = new Board(playerInfo.player1, playerInfo.player2);
    for (let y = 0; y < 9; y++) {
      for (let x = 0; x < 9; x++) {
        if (rowBord.board[x][y]) {
          board.board[x][y] = new Koma(rowBord.board[x][y].playerNumber, rowBord.board[x][y].komaType);
        } else {
          board.board[x][y] = '';
        }
      }
    }
    board.komaDai[0] = board.komaDai[0].concat(rowBord.komaDai[0])
    board.komaDai[1] = board.komaDai[1].concat(rowBord.komaDai[1])
    return board
  }
}



export class Game {
  playerNumber: number;
  player1: any;
  player2: any;
  socket: any;
  board: Board;
  updateGameInfo;
  turn: number;
  state: string;
  currentTime: number;
  constructor(playerNumber, player1, player2, socket) {
    this.playerNumber = playerNumber;
    this.player1 = player1;
    this.player2 = player2;
    this.socket = socket;
    this.board = new Board(this.player1, this.player2);
    this.board.initBoard(this.playerNumber);
    this.updateGameInfo = () => { };
  }


  getPlayerInfoCommand() {
    return {
      method: "adjustTime",
      player1: this.board.player1,
      player2: this.board.player2
    };
  }

  start() {
    this.turn = 1;
    this.state = "playing";
    this.currentTime = Date.now();
  }

  updateTimer() {
    if (this.state == "playing") return;
    let oldTime = this.currentTime;
    this.currentTime = Date.now();
    let player = this.getCurrentPlayer();
    player.time -= this.currentTime - oldTime

    if (player.time < 0) {
      this.socket.emit('game', {
        method: "timeout",
        playerNumber: 2 - this.turn % 2
      });
    }
  }

  isPlayerTurn() {
    return (2 - this.turn % 2) == this.playerNumber;
  }

  nextTurn() {
    this.turn++;
  }

  end() {
    this.state = "end";
  }
  getCurrentPlayer() {
    return this.turn % 2 == 1 ? this.board.player1 : this.board.player2;
  }

  synchronizeGameInfo(bord, turn, playerInfo) {
    this.turn = turn;
    this.board = Board.create(bord, playerInfo);
  }
}