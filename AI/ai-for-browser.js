var ai = require('./AI');
var bgraph = require('./BoardGraphic');
var img = require('./ImageManager');
$(document).ready(function () {
    var bot = new ai.BotClient();
    var imageManager = new img.ImageManager();
    var boardGraphic = new bgraph.BoardGraphicManager($('#board'), $('#komadai1'), $('#komadai2'), imageManager);
    boardGraphic.listen(bot);
    bot.start();
});
