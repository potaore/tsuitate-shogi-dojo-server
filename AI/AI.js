var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var Base = require('./Base');
var getRandom = function (array, predicate) {
    if (array.length == 0)
        return;
    predicate(array[Math.floor(array.length * Math.random())]);
};
var GameState = (function () {
    function GameState() {
    }
    return GameState;
})();
var BotClient = (function (_super) {
    __extends(BotClient, _super);
    function BotClient() {
        _super.call(this, 'bot-client');
        this.state = new GameState();
        this.moveConvertor = new Base.MoveConvertor();
    }
    BotClient.prototype.start = function () {
        var _this = this;
        var socket = require('socket.io-client')('http://133.130.72.92');
        //var socket = require('socket.io-client')('http://localhost:8080');
        socket.on('connect', function () { });
        socket.on('event', function (data) { });
        socket.on('disconnect', function () { });
        socket.on('noticePlayerNumber', function (arg) {
            _this.startGame(arg);
            _this.executeIfMyTurn();
        });
        socket.on('command', function (command) {
            _this.updateGame(command);
            _this.executeIfMyTurn();
        });
        socket.on('synchronize', function (command) {
        });
        socket.on('endGame', function (arg) {
            _this.next(function () {
                if (arg.kifu[arg.kifu.length - 1].info.winPlayerNumber == _this.game.playerNumber) {
                    socket.emit('message', 'あたしの勝ちね！');
                }
                else {
                    socket.emit('message', 'まけちゃった。。。');
                }
                _this.next(function () {
                    socket.emit('room', { method: 'exitRoom' });
                    _this.next(function () {
                        socket.emit('room', { method: 'cancelGame' });
                        _this.next(function () {
                            socket.emit('room', { method: 'startGame', type: 'bot' });
                        });
                    });
                });
            });
        });
        socket.emit('auth.guestlogin.bot', { name: 'Regina (lv2.2)', character: 54 });
        socket.emit('room', { method: 'startGame', type: 'bot' });
        this.socket = socket;
    };
    BotClient.prototype.startGame = function (arg) {
        this.game = new Base.Game(arg.playerNumber, arg.playerInfo.player1, arg.playerInfo.player2, this.socket);
        this.game.start();
        this.state = new GameState();
        if (this.strategy)
            this.strategy.dispose();
        this.strategy = new Strategy(this.game, this.moveConvertor, this.state);
    };
    BotClient.prototype.updateGame = function (commands) {
        var _this = this;
        this.state.foul = false;
        var getKomaFlag = false;
        var outeFlag = false;
        var contradiction = false;
        var removePosition = null;
        var komaType = null;
        commands.forEach(function (command) {
            if (command.method == 'removeKomaFromKomadai') {
                _this.game.board.removeKomaFromKomadai(command.playerNumber, command.koma.komaType);
            }
            else if (command.method == 'putKomaToKomadai') {
                _this.game.board.putKomaToKomadai(command.playerNumber, command.koma);
                komaType = command.koma.komaType;
            }
            else if (command.method == 'removeKoma') {
                _this.game.board.removeKoma(command.position);
                _this.strategy.getKoma(command.position);
            }
            else if (command.method == 'putKoma') {
                _this.game.board.putKoma(command.koma, command.position);
                removePosition = command.position;
            }
            else if (command.method == 'adjustTime') {
                _this.game.board.player1.time = command.player1.time;
                _this.game.board.player2.time = command.player2.time;
            }
            else if (command.method == 'foul') {
                _this.game.getCurrentPlayer().life--;
                _this.state.foul = true;
                _this.strategy.foul();
            }
            else if (command.method == 'noticeOute') {
                outeFlag = command.oute;
            }
            else if (command.method == 'noticeGetKoma') {
                getKomaFlag = command.getKoma;
            }
            else if (command.method == 'contradiction') {
                contradiction = true;
            }
        });
        if (this.game.isPlayerTurn() && getKomaFlag) {
            this.strategy.prediction.converge(komaType, removePosition);
            this.strategy.prediction.setKomadai(komaType);
        }
        else {
        }
        if (!contradiction) {
            if (this.state.foul) {
            }
            else {
                this.state.getKoma = getKomaFlag;
                this.state.oute = outeFlag;
                this.game.nextTurn();
                this.strategy.next();
                if (this.state.oute) {
                    this.strategy.oute();
                }
            }
        }
        else {
            this.socket.emit('game', { method: 'synchronize' });
        }
    };
    BotClient.prototype.executeIfMyTurn = function () {
        var _this = this;
        if (this.game.isPlayerTurn()) {
            this.next(function () { return _this.execute(); });
        }
        this.notify('draw-clear');
        this.notify('draw-koma', this.game);
        this.notify('draw-komadai', this.game);
        this.strategy.prediction.predictionMaps.forEach(function (map) {
            _this.notify('draw-prediction', map);
        });
    };
    BotClient.prototype.execute = function () {
        var moveInfo = this.strategy.execute();
        var action = moveInfo.from.position.x == -1 && moveInfo.from.position.y == -1 ? 'putKoma' : 'moveKoma';
        this.socket.emit('game', {
            method: action,
            args: moveInfo
        });
    };
    BotClient.prototype.next = function (predicate) {
        setTimeout(function () {
            predicate();
        }, 500);
    };
    return BotClient;
})(Artifact.Artifact);
exports.BotClient = BotClient;
var Strategy = (function () {
    function Strategy(game, moveConvertor, state) {
        var _this = this;
        this.isKomaHitted = false;
        this.komaHittedCount = 0;
        this.foulHands = {};
        this.tryPositions = [];
        this.predictionMap_hu = [null, null, null, null, null, null, null, null, null];
        this.moveHisyaInOpening = false;
        this.moveKakuInOpening = false;
        this.moveHuForKakuInOpening = false;
        this.prevGetKoma = false;
        this.useKakuCountDown = 2;
        //駒取り返しを企むフラグ
        this.recapture = false;
        //王手を一度でもかけたフラグ
        this.getOute = false;
        this.suppressionMap = new SuppressionMap();
        this.invatedMap = new SuppressionMap();
        this.foulCount = 0;
        this.game = game;
        this.moveConvertor = moveConvertor;
        this.state = state;
        this.prediction = new Prediction(moveConvertor, game, state, this);
        var dummyGame = new Base.Game(3 - this.game.playerNumber, {}, {}, {});
        Base.util.allPos(function (x, y) {
            var koma = dummyGame.board.getKoma({ x: x, y: y });
            if (koma) {
                var map = _this.prediction.setKoma(koma.komaType, { x: x, y: y });
                if (koma.komaType == 'hu') {
                    _this.predictionMap_hu[x] = map;
                }
            }
        });
        this.predictionMap_ou = this.prediction.predictionMaps.filter(function (map) { return map.komaType == 'ou'; })[0];
        /*
        this.predictionMap_ou.clearBoard();
        if (this.game.playerNumber == 1) {
          this.predictionMap_ou.setPostion({ x: 4, y: 0 });
        } else {
          this.predictionMap_ou.setPostion({ x: 4, y: 8 });
        }
        */
    }
    Strategy.prototype.dispose = function () {
        this.game = undefined;
        this.foulHands = undefined;
        this.tryPositions = undefined;
        this.moveInfoCache = undefined;
        this.prevMoveInfo = undefined;
        this.moveConvertor = undefined;
        this.invatedPosition = undefined;
        this.predictionMap_ou.dispose();
        this.predictionMap_ou = undefined;
        this.suppressionMap = undefined;
        this.invatedMap = undefined;
    };
    Strategy.prototype.foul = function () {
        this.foulHands[JSON.stringify(this.moveInfoCache)] = true;
        this.foulCount++;
        if (this.moveInfoCache.from.komaType == 'ou') {
            this.invatedMap.surpress(this.moveInfoCache.to.position);
        }
    };
    Strategy.prototype.next = function () {
        var _this = this;
        this.foulHands = {};
        this.tryPositions = [];
        this.useKakuCountDown -= 1;
        if (this.game.isPlayerTurn()) {
            this.prediction.moveProbability();
        }
        else {
            this.prevGetKoma = this.state.getKoma;
            this.prevMoveInfo = this.moveInfoCache;
            this.toMoveInfo(this.game.board.getPlayerKoma(this.game.playerNumber), true).forEach(function (moveInfo) {
                _this.suppressionMap.surpress(moveInfo.to.position);
                _this.suppressionMap.surpress(moveInfo.from.position);
            });
        }
    };
    Strategy.prototype.oute = function () {
        if (!this.game.isPlayerTurn()) {
            this.getOute = true;
            console.log('oute');
            var movablePositions = this.moveConvertor.getMovablePos(this.game.playerNumber, this.moveInfoCache.to.komaType, this.moveInfoCache.to.position, this.game.board);
            this.predictionMap_ou.setPositions(movablePositions);
        }
    };
    Strategy.prototype.getKoma = function (position) {
        this.invatedPosition = position;
    };
    Strategy.prototype.isOpening = function () {
        if (this.game.turn < 8)
            return true;
        if (this.game.turn > 26)
            return false;
        var isOpening = 45;
        var isntOpening = this.game.turn * 4;
        if (this.isKomaHitted)
            isntOpening += 5;
        isntOpening += this.komaHittedCount * 2;
        return Math.random() < (isOpening / (isOpening + isntOpening));
    };
    Strategy.prototype.execute = function () {
        var _this = this;
        var _komaArray = this.game.board.getPlayerKoma(this.game.playerNumber);
        var komaArray = this.game.board.getPlayerKoma(this.game.playerNumber);
        var _komadaiArray = this.game.board.getPlayerKomadai(this.game.playerNumber);
        var komadaiArray = this.game.board.getPlayerKomadai(this.game.playerNumber);
        var moveInfoArray = null;
        var executed = false;
        var result = null;
        var count = 0;
        while (!executed) {
            count++;
            var senpou = '';
            var confirmed = false;
            if (count > 15) {
                senpou = 'なにをしたら良いかわからない';
                moveInfoArray = this.toMoveInfo(_komaArray);
                confirmed = true;
            }
            else if (this.state.oute) {
                //王手時の対応
                senpou = '王手時の対応';
                //とりあえず駒は打たない
                komadaiArray = [];
                if (this.state.getKoma) {
                    if (Math.random() > 0.2) {
                        //取り返す
                        moveInfoArray = this.toMoveInfo(_komaArray);
                        moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
                        confirmed = this.existsNotFoulHand(moveInfoArray);
                    }
                    if (!confirmed) {
                        //王を動かす
                        komaArray = this.use('ou', _komaArray);
                        moveInfoArray = this.toMoveInfo(komaArray);
                        confirmed = this.existsNotFoulHand(moveInfoArray);
                    }
                }
                if (!confirmed) {
                    //反則回数が多い状態では王を動かすことを優先する
                    if (Math.random() > 0.6 - 0.06 * this.foulCount) {
                        //王を動かす
                        komaArray = this.use('ou', _komaArray);
                        moveInfoArray = this.toMoveInfo(komaArray);
                        //一度でも相手の利きのあった場所には移動しない
                        var newMoveInfoArray = this.dontMoveToInvatedArea(moveInfoArray);
                        if (newMoveInfoArray.length > 0) {
                            moveInfoArray = newMoveInfoArray;
                        }
                        confirmed = this.existsNotFoulHand(moveInfoArray);
                    }
                    if (!confirmed) {
                        //王以外を動かす
                        var ou = this.game.board.getOu(this.game.playerNumber);
                        //王の周りに駒を動かす
                        var arroundPostions = Base.util.getArroundPos(ou.position);
                        komaArray = this.dontUse('ou', _komaArray);
                        moveInfoArray = this.toMoveInfo(komaArray);
                        moveInfoArray = this.moveToPositions(arroundPostions, moveInfoArray);
                        //一度試した場所は除外する（空き王手がらみがあるので本当はこの処理はだめ）
                        var newMoveInfoArray = this.dontMoveToPositions(this.tryPositions, moveInfoArray);
                        if (newMoveInfoArray.length > 0) {
                            moveInfoArray = newMoveInfoArray;
                        }
                        confirmed = true;
                    }
                }
            }
            if (!confirmed && this.state.getKoma) {
                this.recapture = false;
                //駒を取られた場合の対応
                senpou = '駒を取られた場合の対応';
                //とりあえず駒は打たない
                komadaiArray = [];
                //取り返す
                moveInfoArray = this.toMoveInfo(_komaArray);
                moveInfoArray = this.moveToPosition(this.invatedPosition, moveInfoArray);
                if (moveInfoArray.length >= 2 && Math.random() > 0.01) {
                    //２種類以上の取り方が存在している場合:ほぼ確実に取る
                    confirmed = true;
                    komadaiArray = [];
                }
                else if (moveInfoArray.length == 1 && (Math.random() > 0.2 || moveInfoArray[0].from.komaType == 'hu')) {
                    //取り方が1種類:だいたいとる
                    confirmed = true;
                    komadaiArray = [];
                }
                if (!confirmed && Math.random() > 0.5) {
                    //安い駒で取り返せるように駒を動かす
                    console.log('安い駒で取り返せるように駒を動かす');
                    komaArray = this.dontUse('ou', _komaArray.concat(_komadaiArray));
                    komaArray = this.dontUse('hi', komaArray);
                    komaArray = this.dontUse('ka', komaArray);
                    komaArray = this.dontUse('ki', komaArray);
                    komaArray = this.dontUse('gi', komaArray);
                    moveInfoArray = this.toMoveInfo(komaArray);
                    moveInfoArray = this.targetToPosition(this.invatedPosition, moveInfoArray);
                    if (moveInfoArray.length > 0) {
                        confirmed = true;
                        komadaiArray = [];
                        this.recapture = true;
                        this.recapturePosition = this.invatedPosition;
                    }
                }
            }
            if (!confirmed && this.recapture) {
                //取り返す
                confirmed = true;
                this.recapture = false;
                moveInfoArray = this.toMoveInfo(_komaArray);
                moveInfoArray = this.moveToPosition(this.recapturePosition, moveInfoArray);
            }
            if (!confirmed && this.prevGetKoma && !this.state.getKoma && Math.random() > 0.2) {
                //駒を取った直後の対応
                senpou = '駒を取った直後の対応';
                if (Math.random() > 0.5) {
                    //指定した場所から動かす(取り返されないように駒をかわす)
                    moveInfoArray = this.toMoveInfo(_komaArray);
                    moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
                }
                else {
                    //駒にひもをつける
                    moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
                    moveInfoArray = this.targetToPosition(this.prevMoveInfo.to.position, moveInfoArray);
                    if (moveInfoArray.length == 0) {
                        //指定した場所から動かす(取り返されないように駒をかわす)
                        moveInfoArray = this.toMoveInfo(_komaArray);
                        moveInfoArray = this.moveFromPosition(this.prevMoveInfo.to.position, moveInfoArray);
                    }
                }
                confirmed = true;
            }
            if (!confirmed && this.isOpening()) {
                //序盤の対応
                senpou = '序盤の対応';
                if (this.game.turn > 4 && Math.random() > 0.65) {
                    //王を動かす
                    komaArray = this.use('ou', _komaArray);
                    moveInfoArray = this.toMoveInfo(komaArray);
                    //5筋から離れる
                    moveInfoArray = this.leaveCenterColumn(moveInfoArray);
                    //後退しない
                    moveInfoArray = this.maynotBack(moveInfoArray);
                }
                else {
                    var kaku = this.game.board.get(this.game.playerNumber, 'ka');
                    if (kaku != null && (!this.moveHuForKakuInOpening || !this.moveKakuInOpening) && Math.random() > 0.2) {
                        //飛車先の特攻に備えて角を逃がす
                        if (!this.moveHuForKakuInOpening) {
                            //角道を開ける歩を動かす
                            this.moveHuForKakuInOpening = true;
                            komaArray = this.use('hu', _komaArray);
                            komaArray = komaArray.filter(function (koma) { return koma.position.x == kaku.position.x + 1 || koma.position.x == kaku.position.x - 1; });
                        }
                        else {
                            //角を動かす
                            this.moveKakuInOpening = true;
                            komaArray = this.use('ka', _komaArray);
                        }
                        //候補手の展開
                        moveInfoArray = this.toMoveInfo(komaArray);
                        //角を動かすのは3マスまで
                        moveInfoArray = this.moveShort(moveInfoArray, 3);
                    }
                    else {
                        //王を使わない
                        komaArray = this.dontUse('ou', _komaArray);
                        //香車を使わない
                        komaArray = this.dontUse('ky', komaArray);
                        //序盤は一度しか飛車を動かさない
                        if (this.moveHisyaInOpening) {
                            console.log(' dontUse hi');
                            komaArray = this.dontUse('hi', komaArray);
                        }
                        //序盤は一度しか角を動かさない
                        if (this.moveKakuInOpening) {
                            console.log(' dontUse ka');
                            komaArray = this.dontUse('ka', komaArray);
                        }
                        //候補手の展開
                        moveInfoArray = this.toMoveInfo(komaArray);
                        //金銀は繰り出す
                        moveInfoArray = this.advanceKinGin2(moveInfoArray);
                        //飛車は端に移動しない
                        moveInfoArray = this.dontMoveHisyaToEnd(moveInfoArray);
                    }
                }
                confirmed = true;
            }
            if (!confirmed) {
                //その他の場合
                senpou = 'その他の場合';
                if (Math.random() > 0.5) {
                    //王を使わない
                    komaArray = this.dontUse('ou', _komaArray);
                }
                else {
                    //角を闇雲に動かさない
                    komaArray = this.dontUse('ka', _komaArray);
                }
                //反則回数が多いときは持ち駒の使用を控える
                if (this.foulCount > 7) {
                    moveInfoArray = this.toMoveInfo(komaArray);
                }
                else {
                    moveInfoArray = this.toMoveInfo(komaArray.concat(_komadaiArray));
                }
                if (this.game.turn < 35 && Math.random() > 0.5) {
                    console.log('混合戦略0：飛車先の歩を突く or 最前線の歩を突く');
                    //飛車先の歩を突く
                    var moveInfoArray0 = [];
                    var hisya = this.game.board.get(this.game.playerNumber, 'hi');
                    if (hisya != null) {
                        moveInfoArray0 = this.proceedHu(moveInfoArray, hisya.position.x);
                    }
                    komaArray = this.dontUseEnd(_komaArray);
                    komaArray = this.useTop('hu', komaArray);
                    moveInfoArray = moveInfoArray0.concat(this.toMoveInfo(komaArray));
                }
                else if (Math.random() > 0.4) {
                    console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
                    //制圧済み領域を広げる(まだ王手をかけたことがなければ)
                    var moveInfoArray1 = [];
                    if (!this.getOute) {
                        moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);
                    }
                    if (Math.random() > 0.4) {
                        //金銀を繰り出す
                        var moveInfoArray2 = this.advanceKinGin(moveInfoArray);
                        moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
                    }
                    else {
                        moveInfoArray = moveInfoArray1;
                    }
                }
                else {
                    console.log('混合戦略2：と金をつくる or 王の探索効率が良い手を指す');
                    //と金をつくる手
                    var moveInfoArray_tokin = this.makeTokin(moveInfoArray);
                    //探索範囲の最大の手
                    var moveInfoArray_most = this.mostEffective(moveInfoArray, 5);
                    moveInfoArray = moveInfoArray_tokin.concat(moveInfoArray_most);
                    if (moveInfoArray.length == 0) {
                        console.log('混合戦略1：制圧済み領域を広げる or 金銀を繰り出す');
                        //制圧済み領域を広げる
                        var moveInfoArray1 = this.expandSuppressedArea(moveInfoArray);
                        if (Math.random() > 0.4) {
                            //金銀を繰り出す
                            var moveInfoArray2 = this.advanceKinGin(moveInfoArray);
                            moveInfoArray = moveInfoArray1.concat(moveInfoArray2);
                        }
                        else {
                            moveInfoArray = moveInfoArray1;
                        }
                    }
                }
                //反則回数が多いときは駒の長距離移動は避ける
                if (this.foulCount > 6) {
                    moveInfoArray = this.moveShort(moveInfoArray, 2);
                }
                if (moveInfoArray.length == 0) {
                    console.log('-> 予備：金銀は繰り出す');
                    moveInfoArray = this.toMoveInfo(_komaArray.concat(_komadaiArray));
                    moveInfoArray = this.advanceKinGin2(moveInfoArray);
                }
            }
            moveInfoArray = this.dontRedoFoulHand(moveInfoArray);
            getRandom(moveInfoArray, function (moveInfo) {
                _this.moveInfoCache = moveInfo;
                result = moveInfo;
                executed = true;
                console.log(_this.game.turn + ' : ' + senpou);
                console.log(JSON.stringify(moveInfo));
                console.log('他候補 : ' + moveInfoArray.length);
                if (senpou == '序盤の対応' && moveInfo.from.komaType == 'hi') {
                    _this.moveHisyaInOpening = true;
                }
                if (senpou == '王手時の対応' && moveInfo.from.komaType != 'ou') {
                    _this.tryPositions.push(moveInfo.to.position);
                }
                if (moveInfo.to.komaType == 'ka') {
                    _this.useKakuCountDown = 8;
                }
            });
        }
        return result;
    };
    Strategy.prototype.toMoveInfo = function (komaArray, short) {
        var _this = this;
        if (short === void 0) { short = false; }
        var result = [];
        komaArray.forEach(function (koma) {
            var isKomadai = _this.isKomadai(koma.position);
            var movablePosArray = isKomadai
                ? _this.moveConvertor.getPutableCell(_this.game.playerNumber, koma.koma.komaType, _this.game.board)
                : short
                    ? _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, koma.koma.komaType, koma.position, _this.game.board)
                    : _this.moveConvertor.getMovablePos(_this.game.playerNumber, koma.koma.komaType, koma.position, _this.game.board);
            movablePosArray.forEach(function (movablePos) {
                var moveInfo = {
                    from: {
                        position: koma.position,
                        komaType: koma.koma.komaType
                    },
                    to: {
                        position: movablePos,
                        komaType: koma.koma.komaType
                    }
                };
                if (!isKomadai) {
                    var nari = _this.moveConvertor.getNari(koma.koma.playerNumber, koma.koma.komaType, moveInfo.from.position, moveInfo.to.position);
                    if (nari == 'force' || nari == 'possible') {
                        moveInfo.to.komaType = Base.komaTypes[moveInfo.to.komaType].nariId;
                    }
                }
                result.push(moveInfo);
            });
        });
        return result;
    };
    Strategy.prototype.toMovablePositions = function (moveInfo) {
        return this.moveConvertor.getMovablePos(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
    };
    //座標が(-1, -1)であるかどうかを判定する
    Strategy.prototype.isKomadai = function (position) {
        return position.x == -1 && position.y == -1;
    };
    //前に進める動きかどうかを判定する
    Strategy.prototype.isAdvance = function (moveInfo) {
        if (this.game.playerNumber == 1) {
            return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0;
        }
        else {
            return !this.isKomadai(moveInfo.from.position) && Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0;
        }
    };
    //特定の駒を使わない
    Strategy.prototype.dontUse = function (komaType, komaArray) {
        return komaArray.filter(function (koma) { return koma.koma.komaType != komaType; });
    };
    //特定の駒を使う
    Strategy.prototype.use = function (komaType, komaArray) {
        return komaArray.filter(function (koma) { return koma.koma.komaType == komaType; });
    };
    //最前線の駒を使う
    Strategy.prototype.useTop = function (komaType, komaArray) {
        var newKomaArray = komaArray.filter(function (koma) { return koma.koma.komaType == komaType; });
        if (newKomaArray.length <= 1) {
            return newKomaArray;
        }
        else {
            if (this.game.playerNumber == 1) {
                return [newKomaArray.sort(function (k1, k2) { return k1.position.y - k2.position.y; })[0]];
            }
            else {
                return [newKomaArray.sort(function (k1, k2) { return k2.position.y - k1.position.y; })[0]];
            }
        }
    };
    //角を闇雲に動かさない
    Strategy.prototype.dontUseKakuMany = function (komaArray) {
        if (this.useKakuCountDown <= 0) {
            return komaArray;
        }
        else {
            return this.dontUse('kaku', komaArray);
        }
    };
    //端の駒を使わない
    Strategy.prototype.dontUseEnd = function (komaArray) {
        return komaArray.filter(function (koma) { return koma.position.x != 0 && koma.position.x != 8; });
    };
    //後退しない
    Strategy.prototype.dontBack = function (moveInfoArray) {
        var _this = this;
        if (this.game.playerNumber == 1) {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y < 0; });
        }
        else {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y > 0; });
        }
    };
    //あまり後退しない
    Strategy.prototype.maynotBack = function (moveInfoArray) {
        var _this = this;
        if (Math.random() > 0.5)
            return moveInfoArray;
        if (this.game.playerNumber == 1) {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y <= 0; });
        }
        else {
            return moveInfoArray.filter(function (moveInfo) { return _this.isKomadai(moveInfo.from.position) || Base.util.posSub(moveInfo.to.position, moveInfo.from.position).y >= 0; });
        }
    };
    //5筋から離れる
    Strategy.prototype.leaveCenterColumn = function (moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) {
            if (moveInfo.from.position.x == 4) {
                return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x != 0;
            }
            else if (moveInfo.from.position.x > 4) {
                return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x >= 0;
            }
            else {
                return Base.util.posSub(moveInfo.to.position, moveInfo.from.position).x <= 0;
            }
        });
    };
    //指定した場所から動かす
    Strategy.prototype.moveFromPosition = function (postion, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return Base.util.posEq(moveInfo.from.position, postion); });
    };
    //指定した場所から動かす
    Strategy.prototype.moveFromPositions = function (positions, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return positions.some(function (position) { return Base.util.posEq(moveInfo.from.position, position); }); });
    };
    //指定した場所に動かす
    Strategy.prototype.moveToPosition = function (postion, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return Base.util.posEq(moveInfo.to.position, postion); });
    };
    //指定した場所に動かす2
    Strategy.prototype.moveToPositions = function (positions, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return positions.some(function (position) { return Base.util.posEq(moveInfo.to.position, position); }); });
    };
    //指定した場所に動かさない
    Strategy.prototype.dontMoveToPosition = function (postion, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return !Base.util.posEq(moveInfo.to.position, postion); });
    };
    //指定した場所に動かさない2
    Strategy.prototype.dontMoveToPositions = function (positions, moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return !positions.some(function (position) { return Base.util.posEq(moveInfo.to.position, position); }); });
    };
    //一度反則になった手を指さない
    Strategy.prototype.dontRedoFoulHand = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            return !_this.foulHands.hasOwnProperty(JSON.stringify(moveInfo));
        });
    };
    //反則になった手以外の手があるかどうか
    Strategy.prototype.existsNotFoulHand = function (moveInfoArray) {
        var _this = this;
        return !moveInfoArray.every(function (moveInfo) {
            return _this.foulHands.hasOwnProperty(JSON.stringify(moveInfo));
        });
    };
    //移動距離を制限する
    Strategy.prototype.moveShort = function (moveInfoArray, length) {
        return moveInfoArray.filter(function (moveInfo) {
            return Math.abs(moveInfo.from.position.x - moveInfo.to.position.x) <= length
                && Math.abs(moveInfo.from.position.y - moveInfo.to.position.y) <= length;
        });
    };
    //一度でも相手の利きのあった場所には移動しない
    Strategy.prototype.dontMoveToInvatedArea = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            return !_this.invatedMap.hasSuppressed(moveInfo.to.position);
        });
    };
    //指定した場所にひもがつくようにする
    Strategy.prototype.targetToPosition = function (position, moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            var movablepositions = _this.toMovablePositions(moveInfo);
            return movablepositions.some(function (p) { return Base.util.posEq(p, position); });
        });
    };
    //制圧済み領域が広がる手を指す
    Strategy.prototype.expandSuppressedArea = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            return _this.toMovablePositions(moveInfo).some(function (p) { return !_this.suppressionMap.hasSuppressed(p); });
        });
    };
    //王の探索範囲が変わらない手を指さない
    Strategy.prototype.dontMoveUseless = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            var movablePositions = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, _this.game.board);
            var sum = 0;
            movablePositions.forEach(function (p) {
                sum += _this.predictionMap_ou.board[p.y][p.x];
            });
            return sum != 0;
        });
    };
    //王の探索効率が最も良い手を指す
    Strategy.prototype.mostEffective = function (moveInfoArray, rank) {
        var _this = this;
        var top = 0;
        var topMoveInfo = null;
        var sorted = moveInfoArray.sort(function (m1, m2) {
            var movablePositions1 = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, m1.to.komaType, m1.to.position, _this.game.board);
            var sum1 = 0;
            movablePositions1.forEach(function (p) {
                sum1 += _this.predictionMap_ou.board[p.y][p.x];
            });
            var movablePositions2 = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, m2.to.komaType, m2.to.position, _this.game.board);
            var sum2 = 0;
            movablePositions1.forEach(function (p) {
                sum2 += _this.predictionMap_ou.board[p.y][p.x];
            });
            return sum1 - sum2;
        });
        /*
        moveInfoArray.forEach(moveInfo => {
          let movablePositions = this.moveConvertor.getMovablePosShort(this.game.playerNumber, moveInfo.to.komaType, moveInfo.to.position, this.game.board);
          let sum = 0;
          movablePositions.forEach(p => {
            sum += this.predictionMap_ou.board[p.y][p.x];
          });
          if (sum > top) {
            top = sum;
            topMoveInfo = moveInfo;
          }
        });
        */
        /*
        if (topMoveInfo == null) {
          return [];
        } else {
          return [topMoveInfo];
        }
        */
        if (sorted.length < rank) {
            return sorted;
        }
        else {
            return sorted.slice(0, rank);
        }
    };
    //飛車は端に移動しない
    Strategy.prototype.dontMoveHisyaToEnd = function (moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return moveInfo.from.komaType != 'hi' || (moveInfo.to.position.x != 0 && moveInfo.to.position.x != 8); });
    };
    //と金つくりの手を抽出する
    Strategy.prototype.makeTokin = function (moveInfoArray) {
        return moveInfoArray.filter(function (moveInfo) { return moveInfo.from.komaType == 'hu' && moveInfo.from.komaType == 'to'; });
    };
    //金銀を繰り出す
    Strategy.prototype.advanceKinGin = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
                return _this.isAdvance(moveInfo);
            }
            return false;
        });
    };
    //金銀は繰り出す
    Strategy.prototype.advanceKinGin2 = function (moveInfoArray) {
        var _this = this;
        return moveInfoArray.filter(function (moveInfo) {
            if (moveInfo.from.komaType == 'ki' || moveInfo.from.komaType == 'gi') {
                return _this.isAdvance(moveInfo);
            }
            return true;
        });
    };
    //特定の筋の歩を突く
    Strategy.prototype.proceedHu = function (moveInfoArray, x) {
        return moveInfoArray.filter(function (moveInfo) { return moveInfo.from.komaType == 'hu' && moveInfo.from.position.x == x; });
    };
    return Strategy;
})();
exports.Strategy = Strategy;
var Prediction = (function () {
    function Prediction(moveConvertor, game, state, strategy) {
        this.predictionMaps = [];
        this.getHuColumns = [false, false, false, false, false, false, false, false, false];
        this.invatedHuColumns = [false, false, false, false, false, false, false, false, false];
        this.moveConvertor = moveConvertor;
        this.game = game;
        this.state = state;
        this.strategy = strategy;
    }
    Prediction.prototype.setKoma = function (komaType, position) {
        var map = new PredictionMap(this, komaType, 3 - this.game.playerNumber, this.moveConvertor, this.game, this.state, this.strategy);
        map.setPosition(position);
        this.predictionMaps.push(map);
        return map;
    };
    Prediction.prototype.setKomadai = function (komaType) {
        var map = new PredictionMap(this, komaType, 3 - this.game.playerNumber, this.moveConvertor, this.game, this.state, this.strategy);
        map.setKomadai();
        this.predictionMaps.push(map);
    };
    Prediction.prototype.moveProbability = function () {
        var existRateBoard = this.getExistRateMap();
        if (!this.state.getKoma) {
            var rate = 1 / this.predictionMaps.length;
            this.predictionMaps.forEach(function (map) {
                map.moveProbability(rate, existRateBoard);
            });
            this.predictionMaps.forEach(function (map) {
                map.normalize();
            });
        }
        else {
            var rateSum = this.predictionMaps.reduce(function (sum, map) { return sum + map.moveProbabilityGetRate(existRateBoard); }, 0);
            this.predictionMaps.forEach(function (map) {
                map.moveProbabilityGet(existRateBoard, rateSum);
            });
            this.predictionMaps.forEach(function (map) {
                map.normalizeWith(rateSum);
            });
        }
    };
    Prediction.prototype.converge = function (komaType, position) {
        console.log('converge : ' + komaType);
        var resourceId = Base.komaTypes[komaType].resourceId;
        var komaps = this.predictionMaps.filter(function (komap) {
            return Base.komaTypes[komap.komaType].resourceId == resourceId
                && komap.board[position.y][position.x] > 0;
        });
        console.log('komaps.length : ' + komaps.length);
        if (komaps.length == 1) {
            this.predictionMaps = this.predictionMaps.filter(function (komap) {
                return Base.komaTypes[komap.komaType].resourceId != resourceId
                    || komap.board[position.y][position.x] == 0;
            });
        }
        else {
            var rateSum = komaps.reduce(function (prev, komap) { return prev + komap.board[position.y][position.x]; }, 0);
            komaps.forEach(function (komap) {
                komap.getMaybe(komap.board[position.y][position.x] / rateSum);
            });
        }
        if (komaType == 'hu') {
            this.getHuColumns[position.x] = true;
        }
    };
    Prediction.prototype.getExistRateMap = function () {
        var _this = this;
        var board = getNewBoard();
        Base.util.allPos(function (x, y) {
            var notExistRate = _this.predictionMaps.reduce(function (sum, komap) { return sum * (1 - komap.board[y][x]); }, 1);
            board[y][x] = 1 - notExistRate;
        });
        return board;
    };
    return Prediction;
})();
exports.Prediction = Prediction;
var PredictionMap = (function () {
    function PredictionMap(prediction, komaType, playerNumber, moveConvertor, game, state, strategy) {
        this.komadaiRate = 0;
        this.gettedRate = 0;
        this.prediction = prediction;
        this.komaType = komaType;
        this.playerNumber = playerNumber;
        this.komaId = Base.komaTypes[komaType].resourceId;
        this.moveConvertor = moveConvertor;
        this.game = game;
        this.state = state;
        this.strategy = strategy;
    }
    PredictionMap.prototype.dispose = function () {
        this.board = undefined;
        this.moveConvertor = undefined;
        this.game = undefined;
        this.state = undefined;
        this.strategy = undefined;
    };
    PredictionMap.prototype.clearBoard = function () {
        this.board = getNewBoard();
    };
    PredictionMap.prototype.getMaybe = function (rate) {
        this.gettedRate = 1 - (1 - this.gettedRate) * rate;
    };
    PredictionMap.prototype.setKomadai = function () {
        this.board = getNewBoard();
        ;
        this.komadaiRate = 1;
    };
    PredictionMap.prototype.setPosition = function (position) {
        this.clearBoard();
        this.board[position.y][position.x] = 1;
        this.decisionPoint = position;
        this.yRange = { min: position.y, max: position.y };
    };
    PredictionMap.prototype.setPositions = function (positions) {
        var _this = this;
        var board = getNewBoard();
        positions.forEach(function (p) {
            board[p.y][p.x] = _this.board[p.y][p.x] / positions.length;
        });
        this.nextBoard = board;
        this.normalize();
    };
    PredictionMap.prototype.normalize = function () {
        var sum = 0;
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.nextBoard[y][x] < 0.0000000001)
                    this.nextBoard[y][x] = 0;
                sum += this.nextBoard[y][x];
            }
        }
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.nextBoard[y][x] == 0)
                    continue;
                this.nextBoard[y][x] = this.nextBoard[y][x] / sum * (1 - this.gettedRate);
                if (this.nextBoard[y][x] != 0) {
                    if (this.yRange.min > y)
                        this.yRange.min = y;
                    if (this.yRange.max < y)
                        this.yRange.max = y;
                }
            }
        }
        this.board = this.nextBoard;
    };
    PredictionMap.prototype.moveProbabilityGetRate = function (existRateBoard) {
        var _this = this;
        var sum = 0;
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                var movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
                movablePositions.forEach(function (pos) {
                    //香車などによる追い越し禁止
                    if (_this.isPassed(x, y, pos))
                        return;
                    if (Base.util.posEq(pos, _this.strategy.invatedPosition)) {
                        sum += _this.board[y][x];
                    }
                });
            }
        }
        this.rateSum = sum;
        return sum;
    };
    PredictionMap.prototype.moveProbabilityGet = function (existRateBoard, rateSum) {
        var _this = this;
        if (this.rateSum == 0) {
            return;
        }
        var board = getNewBoard();
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                var movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
                movablePositions.forEach(function (pos) {
                    //香車などによる追い越し禁止
                    if (_this.isPassed(x, y, pos))
                        return;
                    if (Base.util.posEq(pos, _this.strategy.invatedPosition)) {
                        board[_this.strategy.invatedPosition.y][_this.strategy.invatedPosition.x] += _this.board[y][x] * rateSum / _this.rateSum;
                    }
                });
                board[y][x] += this.board[y][x] * (1 - this.rateSum / rateSum);
            }
        }
        this.nextBoard = board;
    };
    PredictionMap.prototype.normalizeWith = function (rateSum) {
        if (this.rateSum == 0) {
            return;
        }
        var sum = 0;
        var position = this.strategy.invatedPosition;
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (x == position.x && y == position.y)
                    continue;
                sum += this.nextBoard[y][x];
            }
        }
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.nextBoard[y][x] == 0)
                    continue;
                this.nextBoard[y][x] = this.nextBoard[y][x] / sum * (1 - this.gettedRate) * ((1 - this.rateSum / rateSum));
            }
        }
        this.nextBoard[position.y][position.x] = this.rateSum / rateSum;
        this.board = this.nextBoard;
    };
    PredictionMap.prototype.moveProbability = function (rate, existRateBoard) {
        var _this = this;
        var board = getNewBoard();
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.board[y][x] == 0)
                    continue;
                var movablePositions = this.moveConvertor.getMovablePos(3 - this.game.playerNumber, this.komaType, { x: x, y: y }, this.game.board);
                var rateSum = movablePositions.reduce(function (sum, pos) { return sum + (1 - existRateBoard[pos.y][pos.x]); }, 0);
                if (rateSum != 0) {
                    movablePositions.forEach(function (pos) {
                        //香車などによる追い越し禁止
                        if (_this.isPassed(x, y, pos))
                            return;
                        board[pos.y][pos.x] += rate / (rateSum / (1 - existRateBoard[pos.y][pos.x])) * _this.board[y][x];
                    });
                }
                board[y][x] += this.board[y][x] * (1 - rate);
            }
        }
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (this.game.board.getKoma({ x: x, y: y })) {
                    board[y][x] = 0;
                }
            }
        }
        this.nextBoard = board;
        var komaArray = this.game.board.getPlayerKoma(this.game.playerNumber);
        if (this.komaType == 'ou') {
            komaArray.forEach(function (koma) {
                var movablepositions = _this.moveConvertor.getMovablePosShort(_this.game.playerNumber, koma.koma.komaType, koma.position, _this.game.board);
                movablepositions.forEach(function (p) {
                    _this.nextBoard[p.y][p.x] = 0;
                });
            });
        }
    };
    PredictionMap.prototype.isPassed = function (fromX, fromY, pos) {
        if (!this.decisionPoint)
            return false;
        if (Math.abs(pos.y - fromY) >= 2) {
            if (!this.prediction.getHuColumns[fromX]) {
                var hu = this.strategy.predictionMap_hu[fromX];
                if (this.isBackward(this.decisionPoint.y, hu.decisionPoint.y)) {
                    if (this.isBackward(hu.getMaxY(), pos.y)) {
                        return true;
                    }
                    else {
                    }
                }
            }
        }
        return false;
    };
    PredictionMap.prototype.isBackward = function (targetY, comparisonY) {
        if (this.playerNumber == 1) {
            return targetY >= comparisonY;
        }
        else {
            return targetY <= comparisonY;
        }
    };
    PredictionMap.prototype.getMaxY = function () {
        if (this.playerNumber == 1) {
            return this.yRange.min;
        }
        else {
            return this.yRange.max;
        }
    };
    return PredictionMap;
})();
exports.PredictionMap = PredictionMap;
var SuppressionMap = (function () {
    function SuppressionMap() {
        this.board = this.getNewBoard();
        this.booleanBoard = this.getNewBoolenaBoard();
    }
    SuppressionMap.prototype.getNewBoard = function () {
        return [
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
    };
    SuppressionMap.prototype.getNewBoolenaBoard = function () {
        return [
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false],
            [false, false, false, false, false, false, false, false, false]
        ];
    };
    SuppressionMap.prototype.surpress = function (position) {
        this.board[position.y][position.x] = -1;
        this.booleanBoard[position.y][position.x] = true;
    };
    SuppressionMap.prototype.next = function () {
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                this.board[y][x]++;
            }
        }
    };
    SuppressionMap.prototype.getUnsuppressedPosition = function () {
        var result = [];
        for (var y = 0; y < 9; y++) {
            for (var x = 0; x < 9; x++) {
                if (!this.booleanBoard[y][x]) {
                    result.push({ x: x, y: y });
                }
            }
        }
        return result;
    };
    SuppressionMap.prototype.hasSuppressed = function (position) {
        return this.booleanBoard[position.y][position.x];
    };
    return SuppressionMap;
})();
var getNewBoard = function () {
    return [
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0]
    ];
};
