var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var base = require('./Base');
var BoardCanvas = (function (_super) {
    __extends(BoardCanvas, _super);
    function BoardCanvas(canvas) {
        _super.call(this, 'board-canvas');
        this.eventEffective = true;
        this.canvas = canvas;
        this.canvas.get(0).width = 640;
        this.canvas.get(0).height = 640;
    }
    BoardCanvas.prototype.init = function () {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
        this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
        this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    };
    BoardCanvas.prototype.putKoma = function (image, physicalPosition) {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.globalAlpha = 1;
        this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
    };
    BoardCanvas.prototype.putKomaAlpha = function (image, physicalPosition, alpha) {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.globalAlpha = alpha;
        this.ctx.drawImage(image, 8 + physicalPosition.x * 70, 8 + physicalPosition.y * 70, 60, 64);
    };
    BoardCanvas.prototype.checkCell = function (position) {
        this.ctx.fillStyle = 'rgba(256, 150, 150, 0.5)';
        this.ctx.fillRect(5 + position.x * 70, 5 + position.y * 70, 70, 70);
    };
    return BoardCanvas;
})(Artifact.Artifact);
exports.BoardCanvas = BoardCanvas;
var KomadaiCanvas = (function (_super) {
    __extends(KomadaiCanvas, _super);
    function KomadaiCanvas(canvas, num, imageManager) {
        _super.call(this, 'komadai');
        this.rects = [];
        this.eventEffective = false;
        this.num = num;
        this.canvas = canvas;
        this.imageManager = imageManager;
        this.canvas.get(0).width = 310;
        this.canvas.get(0).height = 64;
    }
    KomadaiCanvas.prototype.init = function () {
        this.ctx = this.canvas.get(0).getContext('2d');
        this.ctx.fillStyle = 'rgba(255, 10, 30, 0)';
        this.ctx.clearRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
        this.ctx.fillRect(0, 0, this.canvas.get(0).width, this.canvas.get(0).height);
    };
    KomadaiCanvas.prototype.drawKoma = function (komalist, flip) {
        var _this = this;
        this.init();
        var tmp = '';
        var list = null;
        var komaTypeList = [];
        komalist.forEach(function (koma) {
            if (tmp != koma.komaType) {
                if (list != null)
                    komaTypeList.push(list);
                list = [koma];
            }
            else {
                list.push(koma);
            }
            tmp = koma.komaType;
        });
        if (list != null)
            komaTypeList.push(list);
        var typeCount = 0;
        var ctx = this.ctx;
        this.rects = [];
        var rects = this.rects;
        komaTypeList.forEach(function (list) {
            var x = 10 + typeCount * 42;
            var y = 10;
            ctx.drawImage(_this.imageManager.getKomaImage(list[0], flip), x, y, 45, 45);
            var rect = { koma: list[0], x: x, width: 42, y: y, height: 45 };
            rects.push(rect);
            if (_this.emphasizeKoma) {
                if (_this.emphasizeKoma.komaType == list[0].komaType) {
                    _this.emphasizeRect(rect);
                    _this.emphasizeKoma = null;
                }
            }
            if (list.length > 1) {
                _this.circle(25 + typeCount * 42, 10, 7);
                _this.ctx.fillStyle = 'rgba(255, 255, 255, 0.99)';
                _this.ctx.font = 'bold 15px "Arial"';
                _this.ctx.textAlign = 'center';
                _this.ctx.fillText(list.length, 25 + typeCount * 42, 15);
            }
            typeCount++;
        });
    };
    KomadaiCanvas.prototype.circle = function (x, y, r) {
        var ctx = this.canvas.get(0).getContext('2d');
        ctx.beginPath();
        ctx.arc(x, y, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = '#0275d8';
        ctx.fill();
        ctx.lineWidth = 5;
        ctx.strokeStyle = '#0275d8';
        ctx.stroke();
    };
    KomadaiCanvas.prototype.checkRect = function (rect) {
        this.ctx.lineWidth = 5;
        this.ctx.strokeStyle = 'rgba(256, 150, 150, 0.5)';
        this.ctx.strokeRect(rect.x, rect.y, rect.width, rect.height);
    };
    KomadaiCanvas.prototype.emphasizeRect = function (rect) {
        this.ctx.lineWidth = 5;
        this.ctx.font = 'bold 15px "Arial"';
        this.ctx.strokeStyle = 'rgba(0, 255, 255, 0.8)';
        this.ctx.strokeRect(rect.x + 2, rect.y, rect.width, rect.height);
    };
    KomadaiCanvas.prototype.emphasize = function (koma) {
        this.emphasizeKoma = koma;
    };
    return KomadaiCanvas;
})(Artifact.Artifact);
exports.KomadaiCanvas = KomadaiCanvas;
var BoardGraphicManager = (function (_super) {
    __extends(BoardGraphicManager, _super);
    function BoardGraphicManager(board, komadai1, komadai2, imageManager) {
        _super.call(this, 'BoardGraphicManager');
        this.params = { flip: false, oute: false, blind: 0 };
        this.memo = { game: null };
        this.playerInfoMemo = null;
        this.gameInfoMemo = null;
        this.moveToPositions = [];
        this.putToPositions = [];
        this.boardCanvas = new BoardCanvas(board);
        this.komadai1Canvas = new KomadaiCanvas(komadai1, 1, imageManager);
        this.komadai2Canvas = new KomadaiCanvas(komadai2, 2, imageManager);
        this.imageManager = imageManager;
        this.addEvents();
        this.listen(this.boardCanvas)
            .listen(this.komadai1Canvas)
            .listen(this.komadai2Canvas);
    }
    BoardGraphicManager.prototype.addEvents = function () {
        var _this = this;
        this.on('draw-clear', function (eventName, sender, predictionMap) { return _this.clearKoma(); });
        this.on('draw-init', function (eventName, sender, predictionMap) { return _this.init(); });
        this.on('draw-koma', function (eventName, sender, game) { return _this.drawKoma(game); });
        this.on('draw-komadai', function (eventName, sender, game) { return _this.drawKomaDai(game); });
        this.on('draw-prediction', function (eventName, sender, predictionMap) { return _this.drawPrediction(predictionMap); });
    };
    BoardGraphicManager.prototype.init = function () {
        this.params = { flip: false, oute: false, blind: 0 };
        this.memo = { game: null };
    };
    BoardGraphicManager.prototype.drawKomaDai = function (arg) {
        var komadai1 = this.params.flip ? this.komadai2Canvas : this.komadai1Canvas;
        var komadai2 = this.params.flip ? this.komadai1Canvas : this.komadai2Canvas;
        komadai1.drawKoma(arg.board.komaDai[0], this.params.flip);
        komadai2.drawKoma(arg.board.komaDai[1], this.params.flip);
    };
    BoardGraphicManager.prototype.drawKoma = function (game) {
        var _this = this;
        base.util.allPos(function (x, y) {
            var koma = game.board.getKoma({ x: x, y: y });
            if (koma) {
                var komaimg = _this.imageManager.getKomaImage(koma, false);
                _this.boardCanvas.putKoma(komaimg, { x: x, y: y });
            }
        });
    };
    BoardGraphicManager.prototype.drawPrediction = function (predictionMap) {
        var _this = this;
        base.util.allPos(function (x, y) {
            var koma = _this.imageManager.getKomaImage({ playerNumber: 3 - predictionMap.game.playerNumber, komaType: predictionMap.komaType }, false);
            _this.boardCanvas.putKomaAlpha(koma, { x: x, y: y }, predictionMap.board[y][x]);
        });
    };
    BoardGraphicManager.prototype.putKoma = function (pos) {
        var moveInfo = {
            to: {
                position: pos,
                komaType: this.putKomaType
            }
        };
    };
    BoardGraphicManager.prototype.clearKoma = function () {
        this.boardCanvas.init();
        this.komadai1Canvas.init();
        this.komadai2Canvas.init();
    };
    BoardGraphicManager.prototype.initializeTableState = function () {
        this.moveToPositions = [];
        this.fromPosition = null;
        this.putToPositions = [];
        this.putKomaType = '';
        this.boardCanvas.init();
    };
    BoardGraphicManager.prototype.setPlayerInfo = function (command) {
        this.playerInfoMemo = command.account;
        domFinder.getPlayerImageDiv(1, this.params.flip).empty();
        domFinder.getPlayerImageDiv(2, this.params.flip).empty();
        domFinder.getPlayerNameDiv(1, this.params.flip).empty();
        domFinder.getPlayerNameDiv(2, this.params.flip).empty();
        domFinder.getPlayerImageDiv(1, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player1.profile_url, this.playerInfoMemo.player1.character));
        domFinder.getPlayerNameDiv(1, this.params.flip).append(this.playerInfoMemo.player1.name);
        domFinder.getPlayerImageDiv(2, this.params.flip).append(this.imageManager.getIconImage(this.playerInfoMemo.player2.profile_url, this.playerInfoMemo.player2.character));
        domFinder.getPlayerNameDiv(2, this.params.flip).append(this.playerInfoMemo.player2.name);
    };
    BoardGraphicManager.prototype.setTurn = function (turn) {
        domFinder.getGameTurnDiv().empty();
        if (turn) {
            domFinder.getGameTurnDiv().append(turn + '手');
        }
        else {
            domFinder.getGameTurnDiv().append('対局開始');
        }
        var playerTurn = turn % 2;
        domFinder.getPlayerNameDiv(1 + playerTurn, this.params.flip).addClass('label-success');
        domFinder.getPlayerNameDiv(playerTurn, this.params.flip).removeClass('label-success');
    };
    BoardGraphicManager.prototype.updateGameInfo = function (command) {
        var emphasize1 = false;
        var emphasize2 = false;
        if (this.gameInfoMemo) {
            emphasize1 = command.player1.life < this.gameInfoMemo.player1.life;
            emphasize2 = command.player2.life < this.gameInfoMemo.player2.life;
        }
        this.gameInfoMemo = {
            player1: {
                time: command.player1.time,
                life: command.player1.life
            },
            player2: {
                time: command.player2.time,
                life: command.player2.life
            }
        };
        domFinder.getPlayerTimeDiv(1, this.params.flip).empty();
        domFinder.getPlayerTimeDiv(2, this.params.flip).empty();
        domFinder.getPlayerLifeLabel(1, this.params.flip).empty();
        domFinder.getPlayerLifeLabel(2, this.params.flip).empty();
        domFinder.getPlayerTimeDiv(1, this.params.flip).append(this.computeDuration(command.player1.time));
        domFinder.getPlayerTimeDiv(2, this.params.flip).append(this.computeDuration(command.player2.time));
        domFinder.getPlayerLifeLabel(1, this.params.flip).append(command.player1.life);
        domFinder.getPlayerLifeLabel(2, this.params.flip).append(command.player2.life);
    };
    BoardGraphicManager.prototype.resetGameInfo = function () {
        if (this.gameInfoMemo)
            this.updateGameInfo(this.gameInfoMemo);
    };
    BoardGraphicManager.prototype.computeDuration = function (ms) {
        if (ms < 0)
            ms = 0;
        var h = parseInt(new String(Math.floor(ms / 3600000) + 100).substring(1));
        var m = parseInt(new String(Math.floor((ms - h * 3600000) / 60000) + 100).substring(1));
        var s = new String(Math.floor((ms - h * 3600000 - m * 60000) / 1000) + 100).substring(1);
        return m + ':' + s;
    };
    return BoardGraphicManager;
})(Artifact.Artifact);
exports.BoardGraphicManager = BoardGraphicManager;
var domFinder = {
    getPlayerNameDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#player-name-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#player-name-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerImageDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#icon-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#icon-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getGameTurnDiv: function () {
        return $('#game-turn-label');
    },
    getPlayerTimeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-time-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-time-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeLabel: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getPlayerLifeDiv: function (playerNumber, flip) {
        if (flip) {
            return $('#left-life-label-div' + (playerNumber === 1 ? "2" : "1"));
        }
        else {
            return $('#left-life-label-div' + (playerNumber === 1 ? "1" : "2"));
        }
    },
    getFlipItem: function () {
        return $('.flip-item');
    },
    getNotFlipItem: function () {
        return $('.not-flip-item');
    }
};
