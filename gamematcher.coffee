util = require("./uuid.js")

class GameMatcher
	constructor : (@gameFactory) ->
		@matchingGame = null
		@matchingGameRating = null
		@matchingGameBot = null
		@playingGames = {}
		@matchingCustomRules = {}

	asignPlayer : ( player, type ) =>
		if Object.keys(@playingGames).length >= 10
			return { result : "upper_limit", asigned : false }
		if type is "rating"
			if @matchingGameRating
				if Math.random() < 0.5
					@matchingGameRating.player2 = player
				else
					@matchingGameRating.player2 = @matchingGameRating.player1
					@matchingGameRating.player1 = player
				game = @matchingGameRating
				@playingGames[game.gameId] = game
				@matchingGameRating = null
				game.game = @gameFactory.createGame(game.player1, game.player2)
				return { result : "start_game", game : game, asigned : true }
			else
				@matchingGameRating = @createGame(player, "rating")
				return { result : "create_game", game : @matchingGameRating, asigned : true }
		else if type is "bot"
			if @matchingGameBot
				if Math.random() < 0.5
					@matchingGameBot.player2 = player
				else
					@matchingGameBot.player2 = @matchingGameBot.player1
					@matchingGameBot.player1 = player
				game = @matchingGameBot
				@playingGames[game.gameId] = game
				@matchingGameBot = null
				game.game = @gameFactory.createGame(game.player1, game.player2)
				return { result : "start_game", game : game, asigned : true }
			else
				if player.bot
					@matchingGameBot = @createGame(player, "free")
					return { result : "create_game", game : @matchingGameBot, asigned : true }
				else
					return { result : "bot_doesnt_exist", asigned : false }
		else
			if @matchingGame
				if Math.random() < 0.5
					@matchingGame.player2 = player
				else
					@matchingGame.player2 = @matchingGame.player1
					@matchingGame.player1 = player
				game = @matchingGame
				@playingGames[game.gameId] = game
				@matchingGame = null
				game.game = @gameFactory.createGame(game.player1, game.player2)
				return { result : "start_game", game : game, asigned : true }
			else
				@matchingGame = @createGame(player, "free")
				return { result : "create_game", game : @matchingGame, asigned : true }

	createCustomRule : ( player, type, custom ) =>
		game = @createGame(player, type, custom)
		@matchingCustomRules[game.gameId] = game
		return { result : "create_game", game : game, asigned : true }

	asignCustomRule : (player, gameId) =>
		if @matchingCustomRules[gameId]
			if Math.random() < 0.5
				@matchingCustomRules[gameId].player2 = player
			else
				@matchingCustomRules[gameId].player2 = @matchingCustomRules[gameId].player1
				@matchingCustomRules[gameId].player1 = player

			if @matchingCustomRules[gameId].custom.teai is 'teai'
				if @matchingCustomRules[gameId].custom.turn is 'player1'
					@matchingCustomRules[gameId].player2 = player
				else
					@matchingCustomRules[gameId].player2 = @matchingCustomRules[gameId].player1
					@matchingCustomRules[gameId].player1 = player

			game = @matchingCustomRules[gameId]
			@playingGames[game.gameId] = game
			delete @matchingCustomRules[gameId]
			game.game = @gameFactory.createGame(game.player1, game.player2, game.custom)
			return { result : "start_game", game : game, asigned : true }
		else
			return { result : "start_game_failed", game : null, asigned : false }



	createGame : (player, type, custom) => { 
		game    : null
		type    : if type is "rating" then "rating" else "free"
		gameId  : util.uuid()
		player1 : player 
		watcher : []
		custom : custom}

	endGame : (gameId) =>
		if @playingGames[gameId]
			delete @playingGames[gameId]
			console.log "delete playing game"
		if @matchingGame and @matchingGame.gameId is gameId
			@matchingGame = null
			console.log "delete matching game"
		if @matchingGameRating and @matchingGameRating.gameId is gameId
			@matchingGameRating = null
			console.log "delete matching game rating"
		if @matchingGameBot and @matchingGameBot.gameId is gameId
			@matchingGameBot = null
			console.log "delete matching game bot"

		if @matchingCustomRules[gameId]
			delete @matchingCustomRules[gameId]
			console.log "delete custom rule"

	discardMachingGame : () => @matchingGameBot = null
	discardMachingGameRating : () => @matchingGameRating = null
	discardMachingGameBot : () => @matchingGameBot = null

module.exports = GameMatcher