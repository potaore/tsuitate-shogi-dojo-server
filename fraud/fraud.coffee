_ = require('underscore')


#超短手数で試合が終わることが多い
checkFraud1 = ( games, tid ) ->
	console.log "checkFraud1"
	p1 = _( games ).filter( (game) -> game.kifuLength < 35 ).length
	p2 = _( games ).filter( (game) -> game.kifuLength < 25 ).length
	p3 = _( games ).filter( (game) -> game.kifuLength < 15 ).length
	p4 = _( games ).filter( (game) -> game.kifuLength < 5  ).length
	console.log (p1 + p2 + p3 + p4)
	return (p1 + p2 + p3 + p4) > 15

#短手数で接続切れ、投了で勝つことが多い
checkFraud2 = ( games, tid ) -> 
	console.log "checkFraud2"
	count = _( games ).filter( (game) -> ( game.kifuLength < 40 ) and ( ( game.reason is "disconnect" ) or ( game.reason is "resign") ) ).length
	console.log (count)
	return 5 < count


#同じ人とばかり対局する、かつ一方的
checkFraud3 = ( games, tid ) -> 
	console.log "checkFraud3"
	map = {}
	_( games ).filter( (game) ->
		oppTid = if game.account.player1.tid is tid then game.account.player2.tid else game.account.player1.tid
		if not map[oppTid]
			 map[oppTid] = 0
		winPlayer = if game.winPlayerNumber is 1 then game.account.player1 else game.account.player2
		if winPlayer.tid is tid
			map[oppTid]++
		else
			map[oppTid]++
	).length
	console.log (map)
	return _(_( map ).keys).some( ( key ) => map[key] > 7 )

checkFroud = ( games, tid ) ->
	point = 0
	_games = games.filter( (game) => game.kifuLength )
	console.log "games : #{_games.length}"
	point++ if checkFraud1(_games, tid)
	point++ if checkFraud2(_games, tid)
	point++ if checkFraud3(_games, tid)
	return point > 2

module.exports.checkFroud = checkFroud

###
checkFroud([{
        "gameId": "b9e0ee6d-b30e-4e23-a065-d9f1e556e3ad",
        "startDateTime": "20151031065808151",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg"
            },
            "player2": {
                "name": "asdf",
                "character": 15
            }
        }
    }, {
        "gameId": "07761faf-40d4-4dd4-8cee-d28d3ae1f79c",
        "startDateTime": "20151121224824375",
        "account": {
            "player1": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png"
            },
            "player2": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg"
            }
        }
    }, {
        "gameId": "71214a17-92b0-4cc4-b95b-008cb4c22b02",
        "startDateTime": "20151121225000349",
        "account": {
            "player1": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png"
            },
            "player2": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg"
            }
        }
    }, {
        "gameId": "3c07babc-b0b5-4883-9e28-cb8ff020b5ef",
        "startDateTime": "20151121230430231",
        "account": {
            "player1": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "rate": 1500
            },
            "player2": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "rate": 1500
            }
        }
    }, {
        "gameId": "d44bf3c6-7626-4789-b622-742c1428d97a",
        "startDateTime": "20151121231635302",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "rate": 1500
            },
            "player2": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "rate": 1500
            }
        }
    }, {
        "gameId": "77adec88-0b3b-43ea-ab92-6789ada31301",
        "startDateTime": "20151121233608630",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "rate": 1490
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "rate": 1500
            }
        }
    }, {
        "gameId": "9b437de3-c201-4247-97e7-0d488e63afbd",
        "startDateTime": "20151123003022406",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 20,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 36,
                "rate": 1509.8
            }
        }
    }, {
        "gameId": "d05db511-198d-4636-807d-e6c4532366bb",
        "startDateTime": "20151212140609785",
        "account": {
            "player1": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 28,
                "rate": 1509.8
            },
            "player2": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 39,
                "rate": 1480.2
            }
        }
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }, {
        "gameId": "7573c094-f549-4736-b4ea-6e632fd46c55",
        "startDateTime": "20151212145748563",
        "account": {
            "player1": {
                "tid": "99582607",
                "name": "potalong_oreo",
                "profile_url": "http://pbs.twimg.com/profile_images/416748216445440000/bA0u8u3W_normal.jpeg",
                "character": 24,
                "rate": 1480.2
            },
            "player2": {
                "tid": "4312288458",
                "name": "potaore3",
                "profile_url": "http://abs.twimg.com/sticky/default_profile_images/default_profile_2_normal.png",
                "character": 22,
                "rate": 1509.8
            }
        },
        "winPlayerNumber": 1,
        "reason": "tsumi",
        "kifuLength": 8
    }], "99582607" )
###