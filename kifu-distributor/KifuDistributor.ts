declare function require(x: string): any;

import Artifact = require('../../potaore/artifact');

import dbClnt = require('../../gametime-core/dbAccessor/dbAccessor');
let express = require('express');
var bodyParser = require('body-parser');

export class KifuDistributor extends Artifact.Artifact {
  started: boolean = false;
  constructor() {
    super('kifu-distributor');
  }

  build(app) {
    let client : dbClnt.DbAccessor = new dbClnt.DbAccessor('http://localhost:10628/', 'gametime', 'game'); 

    if (this.started) return;
    this.started = true;
    
    this.notify('start-listen-find', { path: '/kifu/find' });
    app.get('/kifu/find', (req, res) => {
      this.notify('receive-request-find', { request: req.query });
      client.findSingle(req.query, ret => res.json(ret), ret => res.json({ error : true, message : '棋譜が見つかりませんでした。'}));
    });
  }
}

