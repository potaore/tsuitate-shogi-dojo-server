var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Artifact = require('../../potaore/artifact');
var dbClnt = require('../../gametime-core/dbAccessor/dbAccessor');
var express = require('express');
var bodyParser = require('body-parser');
var KifuDistributor = (function (_super) {
    __extends(KifuDistributor, _super);
    function KifuDistributor() {
        _super.call(this, 'kifu-distributor');
        this.started = false;
    }
    KifuDistributor.prototype.build = function (app) {
        var _this = this;
        var client = new dbClnt.DbAccessor('http://localhost:10628/', 'gametime', 'game');
        if (this.started)
            return;
        this.started = true;
        this.notify('start-listen-find', { path: '/kifu/find' });
        app.get('/kifu/find', function (req, res) {
            _this.notify('receive-request-find', { request: req.query });
            client.findSingle(req.query, function (ret) { return res.json(ret); }, function (ret) { return res.json({ error: true, message: '棋譜が見つかりませんでした。' }); });
        });
    };
    return KifuDistributor;
})(Artifact.Artifact);
exports.KifuDistributor = KifuDistributor;
