express = require('express')
app = express()
fs = require('fs')
#https = require('https')
https = require('http')
socketio = require('socket.io')
Game = require('./game.js')
GameMatcher = require('./gamematcher.js')
_ = require('underscore')
require('date-utils')
log4js = require('log4js')
log4js.configure('server.js.log4js.json')
logger = log4js.getLogger("normal-logger")

AA = require('../gametime-core/account/account-manager-client.js')
accountAccessor = new AA.AccountManagerClient('localhost', '12121')

DA = require('../gametime-core/dbAccessor/dbAccessor.js')
gameDataAccessor = new DA.DbAccessor('http://localhost:10628/', 'gametime', 'game');

KD = require('./kifu-distributor/KifuDistributor.js')

util = require("./uuid.js")

#fraud = require('./fraud/fraud.js')

games = {} 
gameRules = {}
hosts = {}
accountCreateHosts = {}
accounts = {}
cache = 
  latestGames : []
app.use(express.static('../tsuitate-shogi-dojo-client'))
setAccountInfo = (player, account, command) ->
  player.name = account.name
  player.type = "account"
  authed = true
  player.tid = account.tid
  player.character = command.character
  player.profile_url = account.profile_url
  if(not account.profile_url and not player.character)
    player.character = command.subCharacter
  player.rate = account.rate
  player.rank = account.rank
  player.bot = false

filterPlayerInfo = (player) -> { 
  tid : player.tid, name : player.name, 
  profile_url : player.profile_url, 
  character : player.character,
  rate      : player.rate,
  rank      : player.rank }
gameMatcher = new GameMatcher( 
  createGame : (player1, player2, custom) -> new Game( filterPlayerInfo(player1), filterPlayerInfo(player2), custom )
  )

kd = new KD.KifuDistributor()
kd.build(app)
if(process.argv.length < 3)
  server = https.createServer(app).listen(80)
else
  server = https.createServer(app).listen(process.argv[2])

if (process.argv.length < 4)
  allowMultiple = false
else
  allowMultiple = process.argv[3]



messages =
  game_server_error          : "サーバーでエラーが発生しました。対局を中止します。"
  game_upper_limit           : "満室です。"
  game_bot_doesnt_exist      : "AIが対戦中、または稼働していません。"
  game_notfound              : "対局が見つかりませんでした。"
  playing_games_not_found    : "対局情報が見つかりませんでした。"
  watch_history_not_found    : "過去の対局情報が見つかりませんでした。"
  watch_profile_not_found    : "ユーザー情報が見つかりませんでした。"

sessions = {}

io = socketio.listen(server);
g$connectionEvent2 = (io, games, gameMatcher) -> (client) ->
  #console.log client
  #ホスト取得
  host = client.handshake.address

  client.info =
    authed : false 
    player :
      id : client.id
      asigned : false
      sessionId : util.uuid()
  client.on( 'auth.guestlogin', (command) ->
    logger.info( "login str:#{command.name}")
    if hosts[host] && !allowMultiple
      io.sockets.to(client.id).emit( 'auth.login-ng', "ログインできませんでした。" )
      logger.info( "AUTH NG. (multiple connection)" )
      logger.info( "host:#{host}" )
      return
    return if client.info.authed
    name = if command.name then command.name else "no name"
    name = name.split(":")[0]
    client.info.authed = true
    client.info.player.name = name
    client.info.player.type = "guest"
    client.info.player.bot = false
    client.info.player.character = command.character
    g$connectionEvent(io, games, gameMatcher)(client)
    hosts[host] = true
    io.sockets.to(client.id).emit( 'publishSessionId', client.info.player.sessionId )
    io.sockets.to(client.id).emit( 'auth.login-ok', { account : filterPlayerInfo(client.info.player),  message : "ログインしました" } )
  )

  client.on( 'auth.session', (command) ->
    logger.info( "login str:#{command.sessionId}")
    if hosts[host] && !allowMultiple
      logger.info( "AUTH NG. (multiple connection)" )
      logger.info( "host:#{host}" )
      return
    if not sessions[command.sessionId]
      logger.info( "AUTH NG. (session id not found)" )
      logger.info( "host:#{host}" )
      return
    client.info = sessions[command.sessionId]
    client.info.player.id = client.id
    return if client.info.authed
    client.info.authed = true
    g$connectionEvent(io, games, gameMatcher)(client)
    hosts[host] = true
    io.sockets.to(client.id).emit( 'auth.login-ok', { account : filterPlayerInfo(client.info.player),  message : "ログインしました" } )
  )

  client.on( 'auth.guestlogin.bot', (command) ->
    logger.info( "login str:#{command.name}")
    name = if command.name then command.name else "no name"
    name = name.split(":")[0]
    client.info.authed = true
    client.info.player.name = name
    client.info.player.type = "guest"
    client.info.player.bot = true
    client.info.player.character = command.character
    g$connectionEvent(io, games, gameMatcher)(client)
    hosts[host] = true
    io.sockets.to(client.id).emit( 'publishSessionId', client.info.player.sessionId )
    io.sockets.to(client.id).emit( 'auth.login-ok', { account : filterPlayerInfo(client.info.player),  message : "ログインしました" } )
  )

  client.on('auth.login', (command) ->
    logger.info( "login str:#{command.auth}")
    return if not command.auth
    tup = command.auth.split(":")
    return if tup.length isnt 2
    tid = tup[0]
    password = tup[1]
    if hosts[host] && tid isnt "99582607"
      io.sockets.to(client.id).emit( 'auth.login-ng', "ログインできませんでした。" )
      logger.info( "AUTH NG. (multiple connection)" )
      logger.info( "host:#{host}" )
      logger.info( "tid:#{tid}" )
      return

    if accounts[tid] && tid isnt "99582607"
      logger.info( "AUTH NG. Allready login." )
      io.sockets.to(client.id).emit( 'auth.login-ng', "ログインできませんでした。" )
      return

    accountAccessor.login(tid, password, 
      (result) ->
        account = result.data
        logger.info( "AUTH OK" )
        accounts[account.tid] = true
        setAccountInfo(client.info.player, account, command)
        hosts[host] = true
        client.info.player.sessionId = util.uuid()
        g$connectionEvent(io, games, gameMatcher)(client)
        io.sockets.to(client.id).emit( 'publishSessionId', client.info.player.sessionId )
        io.sockets.to(client.id).emit( 'auth.login-ok', { account : filterPlayerInfo(client.info.player),  message : "ログインしました" } )
      () ->
        logger.info( "AUTH NG. Account not found." )
        io.sockets.to(client.id).emit( 'auth.login-ng', "ログインできませんでした。" )
    )
  )

  client.on('auth.create', (command) ->
    logger.info( "create account:#{command.name}")
    logger.info(command)
    return if not command.name
    account =
      name: command.name
      profile_url: ''
      character: command.character
      tid: 'x' + util.uuid().split('-')[0]
      password: util.uuid().split('-')[0]

    if(accountCreateHosts[host])
      io.sockets.to(client.id).emit( 'auth.login-ng', "短期間に複数のアカウントを作成することはできません。" )
      return
    accountAccessor.createAccount(account
      (result) -> 
        io.sockets.to(client.id).emit( 'auth.login-create', { account:account, message : "アカウントを作成しました。以下の認証文字列でログインしてください。"} )
        accountCreateHosts[host] = new Date().getTime()
      (err) -> io.sockets.to(client.id).emit( 'auth.login-ng', "アカウント作成に失敗しました。" )
    );
  );
  
  toUserInfo = (clientitem) => 
    'id'         : clientitem.info.player.tid
    'name'       : clientitem.info.player.name
    'ip'         : clientitem.handshake.address
    'cookie'     : clientitem.handshake.headers.cookie
    'user-agent' : clientitem.handshake.headers['user-agent']

  adminpass = 'aaaabbb'
  client.on('admin:connection-info', (command) =>
    if(command.passphrase isnt adminpass)
      client.disconnect()
      return
    allclients = _(io.sockets.sockets).filter((clientitem) =>
      console.log clientitem
      clientitem.info isnt undefined && clientitem.info isnt null && clientitem.info.player).map(toUserInfo)
    io.sockets.emit( "result",allclients)
    client.disconnect()
  )

  client.on('admin:disconnect-user', (command) =>
    if(command.passphrase isnt adminpass)
      client.disconnect()
      return
    result = _(io.sockets.sockets).filter((clientitem) => clientitem.handshake.address is command.ip)
    _(result).each((clientitem) => clientitem.disconnect())
    io.sockets.emit( "result", _(result).map(toUserInfo))
    client.disconnect()
  )
    
  

g$changeRoom = (io, games, gameMatcher) -> (client, room, watch) ->
  game = gameMatcher.playingGames[client.info.player.watchGameId]
  if game
    game.watcher = _( game.watcher ).filter( (player) -> player.id isnt client.info.player.id )

  loby = room is "loby"
  if loby and client.info.room isnt "loby" and client.info.player.name isnt "silent-XXX"
    client.to(client.info.room).emit('message', 
      type    : "system-info"
      message : "#{client.info.player.name} さんが退室しました。")
  client.leave( client.info.room ) if client.info.room
  client.join( room )
  console.log "#{client.info.player.name} join #{room}"
  client.info.room = room
  if room isnt "loby" and watch and client.info.player.name isnt "silent-XXX"
    client.to(room).emit('message', 
      type    : "system-info"
      message : "#{client.info.player.name} さんが入室しました。")


g$connectionEvent = (io, games, gameMatcher) -> (client) ->
  #logger.debug(client.request)
  changeRoom = g$changeRoom(io, games, gameMatcher)
  sessions[client.info.player.sessionId] = client.info

  #ホスト取得
  host = client.handshake.address
  username = client.info.player.name

  client.on('disconnect',  -> 
    client.info.authed = false
    client.info.player.asigned = false
    hosts[host] = false
    accounts[client.info.player.tid] = false
    changeRoom(client,  "loby")

    if client.info.player.gameId
      game = gameMatcher.playingGames[client.info.player.gameId]
      if game
        playerNumber = -1
        if game.player1 and client.id is game.player1.id
          playerNumber = 1
        if game.player2 and client.id is game.player2.id
          playerNumber = 2
        game.watcher = _( game.watcher ).filter( (player) -> player.id isnt client.info.player.id )
      game = gameMatcher.matchingGame
      if game
        if ( game.player1 and client.id is game.player1.id ) or ( game.player2 and client.id is game.player2.id )
          gameMatcher.endGame(game.gameId)
          return
      game = gameMatcher.matchingGameRating
      if game
        if ( game.player1 and client.id is game.player1.id ) or ( game.player2 and client.id is game.player2.id )
          gameMatcher.endGame(game.gameId)
          return
      game = gameMatcher.matchingGameBot
      if game
        if ( game.player1 and client.id is game.player1.id ) or ( game.player2 and client.id is game.player2.id )
          gameMatcher.endGame(game.gameId)
          return
      if client.info.player.gameId
          gameMatcher.endGame(client.info.player.gameId)
  )

  if client.info.player.gameId
    game = gameMatcher.playingGames[client.info.player.gameId]
    if game
      playerNumber = -1
      if game.player1 and client.info.player.sessionId is game.player1.sessionId
        playerNumber = 1
      if game.player2 and client.info.player.sessionId is game.player2.sessionId
        playerNumber = 2
      console.log playerNumber
      if playerNumber isnt -1
        io.sockets.to(client.id).emit( 'needRestartGame')
  else
    changeRoom(client,  "loby")

  client.on('restartGame', (message) ->
    if client.info.player.gameId
      game = gameMatcher.playingGames[client.info.player.gameId]
      if game
        playerNumber = -1
        if game.player1 and client.info.player.sessionId is game.player1.sessionId
          playerNumber = 1
          game.player1.id = client.id
        if game.player2 and client.info.player.sessionId is game.player2.sessionId
          playerNumber = 2
          game.player2.id = client.id
        console.log playerNumber
        if playerNumber isnt -1
          changeRoom(client, client.info.player.gameId, true)
          io.sockets.to(client.id).emit( 'restartGame', game.game.getPlayerBord(playerNumber) )
  )

  client.on('message', (message) ->
    console.log 'message'
    console.log message
    message = 
      type    : "chat"
      message : username + " : " + message 
    io.sockets.to(client.id).emit( 'message', message )
    client.to(client.info.room).emit('message', message) )

  moveKoma = (command, game) ->
    playerNumber = if game.player1.id is client.info.player.id then 1 else 2
    if game.game.isTurn(playerNumber)
      cache = game.game.turn + JSON.stringify(command)
      return if cache is game.game.moveInfoCache
      game.game.moveInfoCache = cache

      if command.method is 'moveKoma'
        result = game.game.moveKoma(command.args)
      else if command.method is 'putKoma'
        result = game.game.putKoma(command.args)

      if result.allowed 
        console.log "OK"
        io.sockets.to(client.id).emit( 'command', result.ownHistory )
        io.sockets.to( if client.id isnt game.player1.id then game.player1.id else game.player2.id).emit( 'command', result.oppHistory )
        if result.tsumi
          console.log "TSUMI"
          endGame(game)
      else if result.end
        console.log "END"
        endGame(game)
      else if result.contradiction
        console.log "CONTRADICTION"
        io.sockets.to(client.id).emit( 'command', result.ownHistory )
      else
        console.log "NG"
        io.sockets.to(client.id).emit( 'command', result.ownHistory )
        io.sockets.to( if client.id isnt game.player1.id then game.player1.id else game.player2.id).emit( 'command', result.oppHistory )

  client.on('game', (command) ->
    try
      game = games[client.info.player.gameId]
      return if not game 
      return if not game.player1
      return if not game.player2
      playerNumber = 1 if game.player1.id is client.info.player.id
      playerNumber = 2 if game.player2.id is client.info.player.id
      return if playerNumber isnt 1 and playerNumber isnt 2
      if command.method is 'moveKoma' or command.method is 'putKoma'
        moveKoma(command, game)
      else if command.method is 'timeout'
        if game.game.isTurn(command.playerNumber) and game.game.timeout(command.playerNumber)
          console.log "END"
          endGame(game)
      else if command.method is 'resign'
        if game.game.resign(playerNumber)
          console.log "END"
          endGame(game)
      else if command.method is 'synchronize'
        console.log "synchronize"
        io.sockets.to(client.id).emit( 'synchronize', game.game.getPlayerBord(playerNumber) )

    catch error
      logger.error "#################ERROR##################"
      logger.error "event : game"
      logger.error "args" 
      logger.error command
      logger.error error
      logger.error error.stack if error.stack
      logger.error game.player1 if game
      logger.error game.player2 if game
      logger.error "########################################"
      args = 
        type  : "modal"
        texts : [messages.game_server_error]
        error : true
      if game?.player1?.id?
        io.sockets.to(game.player1.id).emit( 'gameMessage', args )
      if game?.player2?.id?
        io.sockets.to(game.player2.id).emit( 'gameMessage', args )
      _(game.watcher).each( (player) ->
        io.sockets.to(player.id).emit( 'gameMessage', args )
      )
      gameMatcher.endGame(game.gameId)
  )

  g$sendMessage = (_game) -> (playerNumber, type, message) ->
    console.log( playerNumber + " " + type + " " + message )
    if playerNumber is 1 or playerNumber is 2
      id = if playerNumber is 1 then _game.player1.id else _game.player2.id
      io.sockets.to(id).emit( 'gameMessage', { type : type, texts : message } )
    else
      _( _game.watcher ).each( (player) -> io.sockets.to(player.id).emit( 'gameMessage', { type : type, texts : message } ) )

  startGame = ( command ) ->
    return if client.info.player.asigned

    if command.method is "createGameRule"
      asignResult = gameMatcher.createCustomRule( client.info.player, command.type, command.custom )
    else if command.method is "asignCustomGameRule"
      asignResult = gameMatcher.asignCustomRule( client.info.player, command.gameId )
    else 
      asignResult = gameMatcher.asignPlayer( client.info.player, command.type )

    if asignResult.result is "upper_limit"
      io.sockets.to(client.id).emit( 'gameMessage', { type  : "modal", texts : [messages.game_upper_limit], error : true } )
    else if asignResult.result is "bot_doesnt_exist"
      io.sockets.to(client.id).emit( 'gameMessage', { type  : "modal", texts : [messages.game_bot_doesnt_exist], error : true } )
    else if asignResult.result is "start_game"
      asignResult.game.startDateTime
      client.info.player.asigned = true
      client.info.player.gameId = asignResult.game.gameId
      io.sockets.to(asignResult.game.player1.id).emit('noticePlayerNumber', { gameId: asignResult.game.gameId, playerNumber : 1, account : asignResult.game.game.getPlayerAccountInfo(), playerInfo : asignResult.game.game.getPlayerInfo() })
      io.sockets.to(asignResult.game.player2.id).emit('noticePlayerNumber', { gameId: asignResult.game.gameId, playerNumber : 2, account : asignResult.game.game.getPlayerAccountInfo(), playerInfo : asignResult.game.game.getPlayerInfo() })
      asignResult.game.game.setMessageSender( g$sendMessage(asignResult.game) )
      asignResult.game.game.setKifuDistributor( (te) ->
        _( asignResult.game.watcher ).each( (player) -> io.sockets.to(player.id).emit( 'distributeKifu', { te : te } ) )
      )
      asignResult.game.game.startGame()
      changeRoom(client, asignResult.game.gameId)
      watchingGame = gameMatcher.playingGames[asignResult.game.player1.watchGameId]
      if watchingGame
        watchingGame.watcher = _( watchingGame.watcher ).filter( (player) -> player.id isnt asignResult.game.player1.id )
      watchingGame = gameMatcher.playingGames[asignResult.game.player2.watchGameId]
      if watchingGame
        watchingGame.watcher = _( watchingGame.watcher ).filter( (player) -> player.id isnt asignResult.game.player2.id )
    else if asignResult.result is "create_game"
      client.info.player.asigned = true
      client.info.player.gameId = asignResult.game.gameId
      games[client.info.player.gameId] = asignResult.game
      changeRoom(client, asignResult.game.gameId)
  
  client.on('room', (command) ->
    try
      if command.method is "startGame"
        startGame( command )
      else if command.method is "createGameRule"
        startGame( command )
      else if command.method is "asignCustomGameRule"
        startGame( command )
      else if command.method is "cancelGame"
        game = gameMatcher.matchingGame
        if game and game.player1 and game.player1.id is client.info.player.id
          game.player1 = null 
          gameMatcher.endGame(game.gameId)
        if game and game.player2 and game.player2.id is client.info.player.id
          game.player2 = null
          gameMatcher.endGame(game.gameId)
        game = gameMatcher.matchingGameRating
        if game and game.player1 and game.player1.id is client.info.player.id
          game.player1 = null 
          gameMatcher.endGame(game.gameId)
        if game and game.player2 and game.player2.id is client.info.player.id
          game.player2 = null
          gameMatcher.endGame(game.gameId)

        _( _(gameMatcher.matchingCustomRules).keys()).each( (gameId) ->
          game = gameMatcher.matchingCustomRules[gameId]
          if game and game.player1 and game.player1.id is client.info.player.id
            game.player1 = null 
            gameMatcher.endGame(game.gameId)
          if game and game.player2 and game.player2.id is client.info.player.id
            game.player2 = null
            gameMatcher.endGame(game.gameId)
        )

        client.info.player.asigned = false
        client.info.player.gameId = null

      else if command.method is "exitRoom"
        game = gameMatcher.playingGames[client.info.player.gameId]
        if game and ( game.game.state isnt "playing" or ( game.player1.id isnt client.info.player.id and game.player2.id isnt client.info.player.id ) )
          client.info.player.asigned = false
          client.info.player.gameId = null
          game.watcher = _( game.watcher ).filter( (player) -> player.id isnt client.info.player.id )
          client.info.player.asigned = false
          client.info.player.gameId = null


        changeRoom(client,  "loby")

      else if command.method is "watch"
        game = gameMatcher.playingGames[command.gameId]
        if game
          return if game.type is "rating"
          if client.id isnt game.player1.id and client.id isnt game.player2.id# and not client.info.player.asigned
            #client.info.player.asigned = true
            changeRoom(client, game.gameId, true)
            game.watcher.push client.info.player
            client.info.player.watchGameId = game.gameId
            console.log client.info.player.watchGameId
            sendkifu = game.game.getKifu();
            sendkifu.gameId = game.gameId;
            io.sockets.to(client.id).emit( 'kifu', { kifu : sendkifu, elapsedTime : game.game.getElapsedTime() } )
        else
          gameDataAccessor.findSingle( { gameId : command.gameId }
              (game) -> 
                changeRoom(client, command.gameId, true)
                io.sockets.to(client.id).emit( 'kifu', { kifu : game } )
              ()     -> 
                io.sockets.to(client.id).emit( 'gameMessage', { type  : "modal", texts : [messages.game_notfound], error : true } )
            )

    catch error
      logger.error "#################ERROR##################"
      logger.error "event : room"
      logger.error "args" 
      logger.error command
      logger.error error
      logger.error error.stack if error.stack
      logger.error game.player1 if game
      logger.error game.player2 if game
      logger.error "########################################"
  )

  client.on('getPlayingGames', (arg) ->
    console.log "broadcastPlayingGames"
    io.sockets.to(client.id).emit( "receivePlayingGames", cache.latestGames )
    client.join("watch")
  )

  client.on('getHistory', (arg) ->
    client.leave("watch")
    if client.info.player.tid
      gameDataAccessor.findDirect( 
        { $orderby : { startDateTime : -1 }, $limit : 105, $or: [ { "account.player1.tid" : client.info.player.tid }, { "account.player2.tid" : client.info.player.tid  } ] }
        ##{ $orderby : { startDateTime : -1 }, $limit : 20, $or: [ { "account.player1.name" : "potaore" }, { "account.player2.name" : "potaore" } ] }
        (result) -> io.sockets.to(client.id).emit( "receivePlayingGames", { playingGames : _(result).map(shapeGame) } )
        (error)  -> io.sockets.to(client.id).emit( "gameMessage", { type  : "modal", texts : [messages.watch_history_not_found], error : false } )
      )
    else
      io.sockets.to(client.id).emit( "gameMessage", { type  : "modal", texts : [messages.watch_history_not_found], error : false } )
  )

  client.on('getProfile', (command) ->
    client.leave("watch")
    if client.info.player.tid
      accountAccessor.getAccount(client.info.player.tid, 
        (result) -> io.sockets.to(client.id).emit( "receiveProfile", { account : result.data } )
      )
    else
      io.sockets.to(client.id).emit( "gameMessage", { type  : "modal", texts : [messages.watch_profile_not_found], error : false } )
  )

  client.on('updateProfile', (command) ->
    
    command.account.name
    if client.info.player.tid
      accountAccessor.getAccount(client.info.player.tid, 
        (result) -> 
          accountAccessor.updateAccount({ 
            tid : client.info.player.tid, 
            name : command.result.data.name,
            password : command.result.data.password 
          },
          () ->
            client.info.player.name = command.account.name
            io.sockets.to(client.id).emit( "updateAccount", { account : filterPlayerInfo(client.info.player),  message : "プロフィールを更新しました。" } )
          )
        )
    else
      io.sockets.to(client.id).emit( "gameMessage", { type  : "modal", texts : [messages.watch_profile_not_found], error : false } )
  )


io.on('connection', g$connectionEvent2(io, games, gameMatcher) )




shapeGame = (game) -> 
  (
    gameId          : game.gameId
    account         : game.account
    type            : game.type
    custom          : game.custom
    startDateTime   : game.startDateTime.replace(/(\d\d\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)(\d\d\d)/, "$2/$3 $4:$5:$6")
    end             : true
    waiting         : false
    )


broadcastConnectionInfo = ->
  try
    #console.log  process.memoryUsage()

    io.sockets.emit( "connectionInfo", 
      connectionCount         : _( io.sockets.sockets ).values().length
      matchingGameCount       : if gameMatcher.matchingGame then 1 else 0
      matchingGameRatingCount : if gameMatcher.matchingGameRating then 1 else 0
      matchingGameBotCount    : if gameMatcher.matchingGameBot then 1 else 0
      playingGameCount        : _( _( gameMatcher.playingGames ).values() ).where((game) -> game.state == "playing").length
    )
    #console.log Object.keys(gameMatcher.playingGames)
    setTimeout( broadcastConnectionInfo, 5000 )   
  catch e
    logger.error(e)
    setTimeout( broadcastConnectionInfo, 5000 )

broadcastConnectionInfo()

broadcastPlayingGames = ->
  try
    result = _( _(gameMatcher.matchingCustomRules).values() ).map( (game) ->
        gameId        : game.gameId
        type          : game.type
        custom        : game.custom
        account       : { player1 : game.player1 }
        startDateTime : ""
        waiting       : true
    )
    result = result.concat(_( _( _( gameMatcher.playingGames ).values() ).filter( (game) -> game.type isnt "rating" ) ).map( (game) ->
        gameId        : game.gameId
        type          : game.type
        custom        : game.custom
        account       : game.game.getPlayerAccountInfo()
        startDateTime : game.game.startDateTime.toLocaleTimeString()
        waiting       : false
    ))

    if result.length < 105
      gameDataAccessor.find( { "__orderby_startDateTime" : -1, "__limit" : 105 - result.length},
          (pastGames) ->
            io.sockets.to("watch").emit( "receivePlayingGames", cache.latestGames = { playingGames : result.concat( _(pastGames).map(shapeGame) ) })
            setTimeout( broadcastPlayingGames, 10000 )
          () ->  
            io.sockets.to("watch").emit( "receivePlayingGames", cache.latestGames = { playingGames : result })
            setTimeout( broadcastPlayingGames, 10000 )
      )
  catch e
    logger.error(e)
    setTimeout( broadcastPlayingGames, 10000 ) 

broadcastPlayingGames()



endGame = (game) ->
  return if not games[game.gameId]
  sendkifu = game.game.getKifu()
  sendkifu.gameId = game.gameId
  io.sockets.to(game.player1.id).emit( 'endGame', sendkifu )
  io.sockets.to(game.player2.id).emit( 'endGame', sendkifu )
  gameData =
    gameId         : game.gameId
    startDateTime  : game.game.startDateTime.toFormat("YYYYMMDDHH24MISSLL")
    account        : game.game.getKifu().account
    playerInfo     : game.game.getKifu().playerInfo
    kifu           : game.game.getKifu().kifu
    custom         : game.game.getKifu().custom
    type           : game.type

  kifu = game.game.getKifu()
  gameDataRef =
    gameId          : game.gameId
    startDateTime   : game.game.startDateTime.toFormat("YYYYMMDDHH24MISSLL")
    account         : game.game.getKifu().account
    winPlayerNumber : kifu.kifu[ kifu.kifu.length-1 ].info.winPlayerNumber
    reason          : kifu.kifu[ kifu.kifu.length-1 ].info.reason
    kifuLength      : "" + kifu.kifu.length
    
  #logger.info( game.game.getKifu().kifu )
  if game.type is "rating"
    game.player1.rate = 1500 if not game.player1.rate
    game.player2.rate = 1500 if not game.player2.rate
    rateFluctuation = 10 - Math.abs( game.player1.rate - game.player2.rate ) * 0.02
    if rateFluctuation < 0.1
      rateFluctuation = 0.1
  gameMatcher.endGame(game.gameId)
  gameStr = JSON.stringify(gameData)
  logger.info("gameStr : " + gameStr)
  gameDataAccessor.insert(gameData, 
    () -> _( [game.player1, game.player2] ).each( (player, i) -> 
      logger.info("player : " + JSON.stringify(player))
      if player.tid
        accountAccessor.getAccount(player.tid, 
          (result) ->
            account = result.data
            logger.info("account : " + JSON.stringify(account))
            account.rate = 1500 if not account.rate
            if game.game.winPlayerNumber is (i+1)
              if game.type is "rating"
                account.results.rating.win++
                account.rate += rateFluctuation
              else
                account.results.free.win++
            else if 3-game.game.winPlayerNumber is (i+1)
              if game.type is "rating"
                account.results.rating.loose++
                account.rate -= rateFluctuation
              else
                account.results.free.loose++
            else
              if game.type is "rating"
                account.results.rating.draw++
              else
                account.results.free.draw++
            accountAccessor.updateAccount({ tid : account.tid, results : account.results, rate : account.rate ,games : [] } ,() ->)
        )
    ),
    (err) -> console.log(err) 
  )




checkTimeout = ->
  try
    _( _( gameMatcher.playingGames ).values() ).each( (game) => 
      if(game.game.timeout(1))
        endGame(game) 
    )
    setTimeout( checkTimeout, 10000 )   
  catch e
    logger.error(e)
    setTimeout( checkTimeout, 10000 )

checkTimeout()



freeAccountCreate = ->
  try
    now = new Date().getTime()
    _( _( accountCreateHosts ).keys() ).each( (host) => 
      time = accountCreateHosts[host]
      if(now - time > 36000000)
        accountCreateHosts[host] = undefined
    )
    setTimeout( freeAccountCreate, 3600000 )   
  catch e
    logger.error(e)
    setTimeout( freeAccountCreate, 3600000 )

freeAccountCreate()