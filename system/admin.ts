declare function require(x: string): any;
declare let process: any;

console.log(process.argv);
let args1 = process.argv.shift();
let args2 = process.argv.shift();
console.log(process.argv);


class AdminClient {
  socket: any;

  constructor() {
    var socket = require('socket.io-client')('http://133.130.72.92');
    //var socket = require('socket.io-client')('http://localhost:8080');

    socket.on('connect', function () { });
    socket.on('event', function (data) { });
    socket.on('disconnect', function () { });
    this.socket = socket;
    this.socket.on('result', (data) => {
      console.log(JSON.stringify(data));
      this.socket.disconnect();
    });
  }

  getLoginUserList() {
    this.socket.emit('admin:connection-info', {
      passphrase: 'aaaabbb'
    });
  }

  disconnectUser(ip) {
    this.socket.emit('admin:disconnect-user', {
      passphrase: 'aaaabbb',
      ip: ip
    });
  }

  next(predicate) {
    setTimeout(() => {
      predicate();
    }, 1000);
  }

}

let bot = new AdminClient();

switch (process.argv[0]) {
  case 'user-list':
    bot.getLoginUserList()
    break;
  case 'disconnect-user':
    bot.disconnectUser(process.argv[1])
    break;
  default:
    break;
}
