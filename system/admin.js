console.log(process.argv);
var args1 = process.argv.shift();
var args2 = process.argv.shift();
console.log(process.argv);
var AdminClient = (function () {
    function AdminClient() {
        var _this = this;
        var socket = require('socket.io-client')('http://133.130.72.92');
        //var socket = require('socket.io-client')('http://localhost:8080');
        socket.on('connect', function () { });
        socket.on('event', function (data) { });
        socket.on('disconnect', function () { });
        this.socket = socket;
        this.socket.on('result', function (data) {
            console.log(JSON.stringify(data));
            _this.socket.disconnect();
        });
    }
    AdminClient.prototype.getLoginUserList = function () {
        this.socket.emit('admin:connection-info', {
            passphrase: 'aaaabbb'
        });
    };
    AdminClient.prototype.disconnectUser = function (ip) {
        this.socket.emit('admin:disconnect-user', {
            passphrase: 'aaaabbb',
            ip: ip
        });
    };
    AdminClient.prototype.next = function (predicate) {
        setTimeout(function () {
            predicate();
        }, 1000);
    };
    return AdminClient;
})();
var bot = new AdminClient();
switch (process.argv[0]) {
    case 'user-list':
        bot.getLoginUserList();
        break;
    case 'disconnect-user':
        bot.disconnectUser(process.argv[1]);
        break;
    default:
        break;
}
